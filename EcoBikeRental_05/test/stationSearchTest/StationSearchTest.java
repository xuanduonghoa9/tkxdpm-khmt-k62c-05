package stationSearchTest;

import com.ebr.bean.Station;
import com.ebr.subsystem.VehicleApi;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StationSearchTest {

    private VehicleApi api = new VehicleApi();
    @Test
    public void testGetAllStations() {
        ArrayList<Station> list= api.getStations(null);
        assertEquals("Error in getStations API!", list.size(), 3);
    }
    @Test
    public void testSearchStationByAddress() {
        ArrayList<Station> list= api.getStations(null);
        assertTrue("No data", list.size() > 0);



        HashMap<String, String> params = new HashMap<String, String>();
        params.put("address", "đại");
        list=api.getStations(params);
        Station station = api.getStations(params).get(0);
        assertEquals("Error",station.getStationName(),"Trường đại học Bách Khoa");



    }
    @Test
    public void testSearchStationByName() {
        ArrayList<Station> list= api.getStations(null);
        assertTrue("No data", list.size() > 0);



        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", "đại");
        list=api.getStations(params);
        Station station = api.getStations(params).get(0);
        assertEquals("Error",station.getStationAddress(),"Trường đại học Bách Khoa");

    }
    
}
