package viewBike;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ BikeApiBlackBoxTest.class, BikeApiWhiteBoxTest.class })
public class BikeApiTestSuite {

}