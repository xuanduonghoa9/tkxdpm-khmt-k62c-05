package viewBike;

import com.ebr.bean.vehicle.Bike;
import com.ebr.subsystem.VehicleApi;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class BikeApiWhiteBoxTest {
    private VehicleApi api = VehicleApi.getInstance();
    @Test
    public void testGetBikes() {
        ArrayList<Bike> list= api.getBikes(null);
        assertEquals("Error in getBikes API!", list.size(), 3);
    }

    @Test(timeout = 1000)
    public void testResponse() {
        api.getBikes(null);
    }
}
