package viewStation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ViewStationBlackBoxTest.class, ViewStationWhiteBoxTest.class })
public class ViewStationTestSuite {

}



