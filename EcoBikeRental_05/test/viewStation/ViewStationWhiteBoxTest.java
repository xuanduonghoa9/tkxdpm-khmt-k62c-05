package viewStation;

import com.ebr.bean.Account;
import com.ebr.controller.user.UserStationPageController;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class ViewStationWhiteBoxTest {
       public UserStationPageController stationPageController = new UserStationPageController();
       
       @Test
       public void testSearch() {
           Map<String, String> searchParams = new HashMap<>();
           searchParams.put("stationName", "Trường đại học Bách Khoa");
           assertEquals("Trường đại học Bách Khoa", stationPageController.search(searchParams).get(0).getStationName());
       }
}
