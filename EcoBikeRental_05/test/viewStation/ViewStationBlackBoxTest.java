package viewStation;

import com.ebr.bean.Account;
import com.ebr.controller.user.UserStationPageController;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class ViewStationBlackBoxTest {
       public UserStationPageController stationPageController = new UserStationPageController();
       
       @Test
       public void testSearch() {
           Map<String, String> searchParams = new HashMap<>();
           assertNotNull(stationPageController.search(searchParams));
       }
}
