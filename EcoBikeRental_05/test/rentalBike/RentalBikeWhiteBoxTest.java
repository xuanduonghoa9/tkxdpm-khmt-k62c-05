package rentalBike;
import com.ebr.bean.Account;
import com.ebr.subsystem.AccountApi;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class RentalBikeWhiteBoxTest {
    private AccountApi accountApi = AccountApi.getInstance();

    @Test
    public void testDeposit() {
        Map<String, String> map = new HashMap<>();
        map.put("cardId", "033099");
        map.put("password", "033099");
        map.put("money", "200000.0");

        Account account = accountApi.getBalance(map);
        assertNotNull("Lỗi thanh toán", account);
    }
    @Test
    public void testDepositEror() {
        Map<String, String> map = new HashMap<>();
        map.put("cardId", "033099");
        map.put("password", null);
        map.put("money", "200000.0");

        Account account = accountApi.getBalance(map);
        assertNull("Lỗi thanh toán", account);
    }
    @Test
    public void testDepositError() {
        Map<String, String> map = new HashMap<>();
        map.put("cardId", "033099");
        map.put("password", "033099");
        map.put("money", "20000000.0");

        Account account = accountApi.getBalance(map);
        assertNull("Lỗi thanh toán", account);
    }
}
