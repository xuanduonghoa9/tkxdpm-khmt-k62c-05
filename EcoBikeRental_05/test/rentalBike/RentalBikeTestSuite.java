package rentalBike;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ RentalBikeBlackBoxTest.class, RentalBikeWhiteBoxTest.class })
public class RentalBikeTestSuite {
}
