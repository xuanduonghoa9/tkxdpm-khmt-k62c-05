package editBike;
import com.ebr.bean.Bike;
import com.ebr.subsystem.VehicleApi;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;
public class EditBikeBlackBoxTest {
	private VehicleApi vehicleApi = VehicleApi.getInstance();

    @Test
    public void testUpdate1() {
        Bike bike = new Bike();
        bike.setCost(0);
        bike.setName("Xe đạp mini Nhật");
        Bike res = vehicleApi.updateBike(bike)
        assertNotNull("Thay đổi thất bại ", res);
    }
    @Test
    public void testUpdate2() {
        Bike bike = new Bike();
        bike.setCost(0);
        Bike res = vehicleApi.updateBike(bike)
        assertNotNull("Thay đổi thất bại ", res);
    }
    @Test
    public void testUpdate3() {
        Bike bike = new Bike();
        bike.setCost(2000);
        Bike res = vehicleApi.updateBike(bike)
        assertNotNull("Thay đổi thất bại ", res);
    }
    @Test
    public void testUpdate4() {
        Bike bike = new Bike();
        bike.setCost(0);
        bike.setName("Xe đạp mini Nhật");
        Bike res = vehicleApi.updateBike(bike)
        assertNull("Thay đổi thành công ", res);
    }
}
