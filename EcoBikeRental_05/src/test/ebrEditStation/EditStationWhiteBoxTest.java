package test.ebrEditStation;

import static org.junit.Assert.assertTrue;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebr.bean.Station;
import com.ebr.controller.admin.AdminStationPageController;
//Phung Ha Duong 20173061
@RunWith(Parameterized.class)
public class EditStationWhiteBoxTest {
	private  Station station;
	private  double expectedResult;
	private AdminStationPageController adminStationPageController = new AdminStationPageController();
	
	public EditStationWhiteBoxTest(String stationId, String stationName, String stationAddress, Integer numberOfBikes,
			Integer numberOfEBikes, Integer numberOfTwinBikes, Integer numberOfEmpty, Integer numberOfDocks, double expectedResult) {
		super();
		this.station = new Station(stationId, stationName, stationAddress, numberOfBikes, numberOfEBikes, numberOfTwinBikes, numberOfEmpty, numberOfDocks);
		
		this.expectedResult = expectedResult;
		
		
	}
	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { 
			{ "station01", "", "Trường đại học Bách Khoa", 10 , 20, 30, 5, 65, 1 },
			{ "station01", null, "Trường đại học Bách Khoa", 10 , 20, 30, 5, 65, 1 },
			{ "station01", "Trường đại học Bách", "", 10 , 20, 30, 5, 65, 1 },
			{ "station01", "Trường đại học Bách Khoa", null , 10 , 20, 30, 5, 65, 1 },
			{ "station01", "Trường đại học Bách Khoa", "Trường đại học Bách Khoa" , -1 , 20, 30, 5, 65, 1 },
			{ "station01", "Trường đại học Bách Khoa", "Trường đại học Bách Khoa" , 10 , -2, 30, 5, 65, 1 },
			{ "station01", "Trường đại học Bách Khoa", "Trường đại học Bách Khoa" , 10 , 20, -3, 5, 65, 1 },
			{ "station01", "Trường đại học Bách Khoa", "Trường đại học Bách Khoa" , 10 , 20, 30, -4, 65, 1 },
			{ "station01", "Trường đại học Bách Khoa", "Trường đại học Bách Khoa" , 10 , 20, 30, 5, -5, 1 },
		});
	}
	
	
	@Test
	public void updateStation() {
	
		adminStationPageController.updateStation(station);
	
	
		assertTrue("Eror in updateBook API!", adminStationPageController.updateStation(station) == null);
	
		
	}
	
}
