package test.ebrEditStation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.ebr.bean.Station;
import com.ebr.controller.admin.AdminStationPageController;
import com.ebr.subsystem.SupportApi;
//Phung Ha Duong 20173061

public class EditStationBlackBoxTest {
	
	private AdminStationPageController adminStationPageController = new AdminStationPageController();
	

	@Test
	public void updateNameStation() {
		ArrayList<Station> list = (ArrayList<Station>) SupportApi.getInstance().getStations(null);
		assertTrue("No data", list.size() > 0);
		
		Station station1 = list.get(0);
		String newStationName = "O'Reilly";
		station1.setStationName(newStationName);
		adminStationPageController.updateStation(station1);
		
	
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("stationName", newStationName);
		list =  SupportApi.getInstance().getStations(params);
		
		assertTrue("Eror in updateBook API!", list.size() > 0);
	
		Station newStation = SupportApi.getInstance().getStations(params).get(0);
		assertEquals("Eror in updateBook API!", newStation.getStationName(), station1.getStationName());
		
	}
	
	@Test
	public void updateAdressStation() {
		ArrayList<Station> list = (ArrayList<Station>) SupportApi.getInstance().getStations(null);
		assertTrue("No data", list.size() > 0);
		
		Station station1 = list.get(0);
		String newStationAddr = "Hoang Mai Ha Noi";
		station1.setStationAddress(newStationAddr);
		adminStationPageController.updateStation(station1);
		
	
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("stationAddress", newStationAddr);
		list =  SupportApi.getInstance().getStations(params);
		
		assertTrue("Erorrr in updateBook API!", list.size() > 0);
	
		Station newStation = SupportApi.getInstance().getStations(params).get(0);
		assertEquals("Eror in updateBook API!", newStation.getStationAddress(), station1.getStationAddress());
		
	}
	
}
