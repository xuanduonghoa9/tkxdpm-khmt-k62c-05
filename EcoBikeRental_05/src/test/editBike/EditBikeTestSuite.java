package test.editBike;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ EditBikeBlackBoxTest.class, EditBikeWhiteBoxTest.class })
public class EditBikeTestSuite {

}
