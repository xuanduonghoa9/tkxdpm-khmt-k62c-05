package test.editBike;
import com.ebr.bean.Bike;
import com.ebr.subsystem.VehicleApi;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;
public class EditBikeWhiteBoxTest {
	private VehicleApi vehicleApi = VehicleApi.getInstance();

    @Test
    public void testUpdate1() {
        Bike bike = new Bike();
        bike.setCost(0.0);
        Bike res = vehicleApi.updateBike(bike);
        assertNull("Thay doi that bai ", res);
    }
    @Test
    public void testUpdate2() {
        Bike bike = new Bike();
        bike.setCost(2000.0);
        bike.setName("Xe đạp mini");
        Bike res = vehicleApi.updateBike(bike);
        assertNotNull("Thay doi thanh cong ", res);
    }
}
