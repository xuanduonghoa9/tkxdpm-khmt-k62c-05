package test.editStation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({ EditStationBlackBoxTest.class, EditStationWhiteBoxTest.class })
public class EditStationTestSuit {

}