package com.ebr.main;

import com.ebr.controller.user.UserStationPageController;
import com.ebr.controller.user.RentalController;

import javax.swing.JPanel;


public class EBRUserController{
    private RentalController rentalController;

    public EBRUserController() {
        super();
        rentalController = RentalController.getInstance();
    }

    public JPanel getStationPage() {
       UserStationPageController controller = new UserStationPageController();
        return controller.getDataPagePane();
    }

}
