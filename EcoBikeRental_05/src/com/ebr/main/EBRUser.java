package com.ebr.main;

import java.awt.BorderLayout;

import javax.swing.*;

@SuppressWarnings("serial")
public class EBRUser extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;
	
	public EBRUser(EBRUserController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);


		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);

		JPanel station = controller.getStationPage();
		tabbedPane.addTab("Xem bãi đỗ xe", null, station, "Xem bãi đỗ xe");
		tabbedPane.addTab("Trả xe", null, new JPanel(), "Trả xe");



		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("EcoBikeRental Nhóm 05 - User");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new EBRUser(new EBRUserController());
			}
		});
	}
}