package com.ebr.main;


import java.awt.BorderLayout;

import javax.swing.*;
//@authors PHUNG HA DUONG_ 20173061
@SuppressWarnings("serial")
public class EBRAdmin extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;
	
	public EBRAdmin(EBRAdminController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);
		
		
		JPanel bikePage = controller.getStationPage();
		
		rootPanel.add(bikePage);
		 

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("EcoBikeRental Nhóm 05 - Admin");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new EBRAdmin(new EBRAdminController());
			}
		});
	}
}