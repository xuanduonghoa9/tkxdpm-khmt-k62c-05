package com.ebr.main;

import javax.swing.JPanel;

import com.ebr.controller.admin.AdminBikePageController;
import com.ebr.controller.admin.AdminStationPageController;


public class EBRAdminController {
	public JPanel getBikePage() {
		AdminBikePageController controller = new AdminBikePageController();
		return controller.getDataPagePane();
	}
	public JPanel getStationPage() {
		AdminStationPageController controller = new AdminStationPageController();
		return controller.getDataPagePane();
	}
}
