package com.ebr.controller.user;

import com.ebr.bean.Account;
import com.ebr.bean.OrderBike;
import com.ebr.views.rentalPane.RentalPane;
import com.ebr.subsystem.AccountApi;
import java.util.HashMap;
import java.util.Map;

public class RentalController {
    private RentalPane rentalPane;
    private OrderBike orderBike;
    private static RentalController instance;
    public static RentalController getInstance(){
        if (instance == null){
            instance = new RentalController();
        }
        return instance;
    }

    public RentalPane getRentalPane() {
        return rentalPane;
    }


    private RentalController() {
        orderBike = new OrderBike();
        rentalPane = new RentalPane();

        rentalPane.setController(this);
        rentalPane.updateData("Bạn chưa thuê xe");
    }

    public void updateRentalPane() {
        rentalPane.updateData(getTextRental());

    }

    public String getTextRental() {
        return "Bạn đã thuê xe ";
    }

    public boolean checkBalance(String cartId, String password, float money) {
        Map<String, String> map = new HashMap<>();
        map.put("cardId", cartId);
        map.put("password", password);
        map.put("money", String.valueOf(money));
        Account account = AccountApi.getInstance().getBalance(map);
        return account != null;
    }

}
