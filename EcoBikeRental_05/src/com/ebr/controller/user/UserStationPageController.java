package com.ebr.controller.user;

import java.util.List;
import java.util.Map;

import com.ebr.bean.Station;
import com.ebr.controller.ADataPageController;
import com.ebr.controller.IDataManageController;
import com.ebr.subsystem.VehicleApi;
import com.ebr.views.listPane.ADataListPane;
import com.ebr.views.listPane.UserStationListPane;
import com.ebr.views.search.ADataSearchPane;
import com.ebr.views.search.StationSearchPane;
import com.ebr.views.singlePane.ADataSinglePane;
import com.ebr.views.singlePane.StationSinglePane;

public class UserStationPageController extends ADataPageController<Station> {
	
	private UserVehiclePagePaneController controller;
	public UserStationPageController() {
		super();
	}

	@Override
	public ADataSearchPane createSearchPane() {
		return new StationSearchPane();
	}

	@Override
	public List<? extends Station> search(Map<String, String> searchParams) {

		return VehicleApi.getInstance().getStations(searchParams);
	}

	@Override
	public ADataSinglePane<Station> createSinglePane() {
		return new StationSinglePane();
	}

	@Override
	public ADataListPane<Station> createListPane() {
		return new UserStationListPane(this);
	}



	public void viewVehicles(Station data, IDataManageController<Station> iDataManageController) {
		// TODO Auto-generated method stub
		UserVehiclePagePaneController userVehiclePagePaneController = new UserVehiclePagePaneController();
		userVehiclePagePaneController.getViewListVehicle(data.getStationId());
	}
}
