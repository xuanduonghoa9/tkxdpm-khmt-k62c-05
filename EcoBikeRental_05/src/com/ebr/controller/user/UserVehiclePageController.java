package com.ebr.controller.user;

import com.ebr.bean.OrderBike;
import com.ebr.bean.vehicle.Vehicle;
import com.ebr.controller.ADataPageController;
import com.ebr.controller.IDataManageController;
import com.ebr.views.listPane.ADataListPane;
import com.ebr.views.listPane.UserVehicleListPane;

import javax.swing.*;

public abstract class UserVehiclePageController extends ADataPageController<Vehicle> {
    public UserVehiclePageController() {
        super();
    }

    @Override
    public ADataListPane<Vehicle> createListPane() {
        return new UserVehicleListPane(this);
    }

    public abstract void rentBike(Vehicle vehicle, OrderBike orderBike, IDataManageController<OrderBike> iDataManageController);

    public abstract JPanel getRentalPane();

    public abstract JPanel viewBike(String stationId);
}
