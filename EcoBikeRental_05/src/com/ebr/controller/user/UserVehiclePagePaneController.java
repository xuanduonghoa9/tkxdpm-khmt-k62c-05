package com.ebr.controller.user;

import com.ebr.views.pagePane.UserVehiclePagePane;

import javax.swing.*;

public class UserVehiclePagePaneController {
    public UserVehiclePagePaneController(){
        super();
    }
    public void getViewListVehicle(String station){
        UserVehiclePagePane pagePane = new UserVehiclePagePane();
        pagePane.getViewListVehicle(station);
    }
}
