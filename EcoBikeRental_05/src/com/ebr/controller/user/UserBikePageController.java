package com.ebr.controller.user;

import com.ebr.bean.Account;
import com.ebr.bean.vehicle.Bike;
import com.ebr.bean.OrderBike;
import com.ebr.bean.vehicle.Vehicle;
import com.ebr.controller.IDataManageController;
import com.ebr.subsystem.AccountApi;
import com.ebr.subsystem.VehicleApi;
import com.ebr.views.search.ADataSearchPane;
import com.ebr.views.search.BikeSearchPane;
import com.ebr.views.singlePane.vehicle.BikeSinglePane;
import com.ebr.views.rentalDialog.RentalDialog;

import javax.swing.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserBikePageController extends UserVehiclePageController {
    private static String stationId;
    private RentalController rentalController = RentalController.getInstance();

    public UserBikePageController() {
        super();
    }

    public UserBikePageController(String stationId) {
        super();
        this.stationId = stationId;
    }

    @Override
    public List<? extends Bike> search(Map<String, String> searchParams) {
        searchParams.put("stationId", stationId);
        return VehicleApi.getInstance().getBikes(searchParams);
    }

    @Override
    public BikeSinglePane createSinglePane() {
        return new BikeSinglePane();
    }

    @Override
    public BikeSearchPane createSearchPane() {
        return new BikeSearchPane();
    }

    public void getBikeStation(String station) {
        ADataSearchPane data = new ADataSearchPane() {
            @Override
            public void buildControls() {
            }

            @Override
            public Map<String, String> getQueryParams() {
                Map<String, String> res = super.getQueryParams();
                if (!station.trim().equals("")) {
                    res.put("stationId", station.trim());
                }
                return res;
            }
        };
    }

    public JPanel viewBike(String stationId) {
        getBikeStation(stationId);
        UserBikePageController controller = new UserBikePageController(stationId);
        return controller.getDataPagePane();
    }

    public JPanel getRentalPane() {
        System.out.println("khoi tao RentalPane: " + rentalController);
        return rentalController.getRentalPane();
    }

    public boolean checkBalance(String cartId, String password, float money) {
        Map<String, String> map = new HashMap<>();
        map.put("cardId", cartId);
        map.put("password", password);
        map.put("money", String.valueOf(money));
        Account account = AccountApi.getInstance().getBalance(map);
        return account != null;
    }

    public void rentBike(Vehicle vehicle, OrderBike orderBike, IDataManageController<OrderBike> iDataManageController) {
        new RentalDialog(vehicle, orderBike, iDataManageController, rentalController);
    }
    public RentalController getRentalController() {
        return rentalController;
    }

    public void setRentalController(RentalController rentalController) {
        this.rentalController = rentalController;
    }
}