package com.ebr.controller.admin;

import com.ebr.views.pagePane.AdminVehiclePagePane;
import com.ebr.views.pagePane.UserVehiclePagePane;

public class AdminVehiclePagePaneController {
    public AdminVehiclePagePaneController(){
        super();
    }
    public void getViewListVehicle(String station){
        AdminVehiclePagePane pagePane = new AdminVehiclePagePane();
        pagePane.getViewListVehicle(station);
    }
}
