package com.ebr.controller.admin;

import com.ebr.bean.vehicle.Vehicle;
import com.ebr.controller.ADataPageController;
import com.ebr.views.listPane.ADataListPane;
import com.ebr.views.listPane.AdminVehicleListPane;

public abstract class AdminVehiclePageController extends ADataPageController<Vehicle> {

	public AdminVehiclePageController() {
		super();
	}
	
	@Override
    public ADataListPane<Vehicle> createListPane() {
        return new AdminVehicleListPane(this);
    }

}
