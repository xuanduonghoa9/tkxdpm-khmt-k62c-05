package com.ebr.controller.admin;

import java.util.List;
import java.util.Map;

import com.ebr.bean.Station;
import com.ebr.controller.ADataPageController;
import com.ebr.controller.IDataManageController;
import com.ebr.subsystem.SupportApi;
import com.ebr.views.editdialog.StationEditDialog;
import com.ebr.views.listPane.ADataListPane;
import com.ebr.views.listPane.AdminStationListPane;
import com.ebr.views.search.StationSearchPane;
import com.ebr.views.singlePane.ADataSinglePane;
import com.ebr.views.singlePane.StationSinglePane;


public class AdminStationPageController extends ADataPageController<Station>{
	
	public AdminStationPageController() {
		super();
	}

	@Override
	public List<? extends Station> search(Map<String, String> searchParams) {
		
		return SupportApi.getInstance().getStations(searchParams);
	}

	public List<? extends Station> updateStation(Station station) {
		 if(station instanceof Station) {
			if(station.getStationAddress() == "" || station.getStationAddress() == null  ) {
				return null;
			}
			if(station.getStationName() == "" || station.getStationName() ==null ) {
				return null;
			}
			if(station.getNumberOfBikes() <0 || station.getNumberOfDocks() <0 
					|| station.getNumberOfEBikes() <0 || station.getNumberOfEmpty() <0
					|| station.getNumberOfTwinBikes() <0 ) {
				return null;
			}
			SupportApi.getInstance().updateStation(station).toString();
		 }
		
		 return SupportApi.getInstance().getStations(null);
	}
	@Override
	public StationSinglePane createSinglePane() {
		return new StationSinglePane();
	}
	@Override
	public StationSearchPane createSearchPane() {
		return new StationSearchPane();
	}

	public void onEdit(ADataSinglePane<Station> singlePane, IDataManageController<Station> iDataManageController ) {
		if(singlePane instanceof StationSinglePane) {
			Station station = singlePane.getData();
			new StationEditDialog(station, iDataManageController);
		}
			
		
	}

	@Override
	public ADataListPane<Station> createListPane() {
		AdminStationListPane listPane =  new AdminStationListPane(this);
		listPane.setController(new IDataManageController<Station>() {

			@Override
			public void onAct(Station t) {
				List<? extends Station> list = AdminStationPageController.this.updateStation(t);
				listPane.updateData(list);
				
			}
			
		});
		return listPane;
	}


	public void viewAdminVehicles(Station data, IDataManageController<Station> iDataManageController) {
		AdminVehiclePagePaneController adminVehiclePagePaneController = new AdminVehiclePagePaneController();
		adminVehiclePagePaneController.getViewListVehicle(data.getStationId());
			
		
	}

	

}

