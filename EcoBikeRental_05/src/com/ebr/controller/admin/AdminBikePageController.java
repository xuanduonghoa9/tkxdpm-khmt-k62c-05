package com.ebr.controller.admin;

import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import com.ebr.bean.vehicle.Vehicle;
import com.ebr.controller.IDataManageController;
import com.ebr.subsystem.VehicleApi;
import com.ebr.views.editdialog.BikeEditDialog;
import com.ebr.views.search.ADataSearchPane;
import com.ebr.views.search.BikeSearchPane;
import com.ebr.views.search.VehicleSearchPane;
import com.ebr.views.singlePane.ADataSinglePane;
import com.ebr.views.singlePane.vehicle.BikeSinglePane;
import com.ebr.views.singlePane.vehicle.VehicleSinglePane;



//@authors PHUNG HA DUONG_ 20173061
public class AdminBikePageController extends AdminVehiclePageController{
	private static String stationId;
	public AdminBikePageController() {
		super();
		
	}
	public AdminBikePageController(String stationId) {
        super();
        this.stationId = stationId;
    }

	@Override
	public List<? extends Vehicle> search(Map<String, String> searchParams) {
		searchParams.put("stationId", stationId);
        return VehicleApi.getInstance().getBikes(searchParams);
	}
	public void getBikeStation(String station) {
        ADataSearchPane data = new ADataSearchPane() {
            @Override
            public void buildControls() {
            }

            @Override
            public Map<String, String> getQueryParams() {
                Map<String, String> res = super.getQueryParams();
                if (!station.trim().equals("")) {
                    res.put("stationId", station.trim());
                }
                return res;
            }
        };
    }
	public JPanel viewBike(String stationId) {
        getBikeStation(stationId);
        AdminBikePageController controller = new AdminBikePageController(stationId);
        return controller.getDataPagePane();
    }
	@Override
	public VehicleSinglePane createSinglePane() {
		return new BikeSinglePane();
	}
	@Override
	public VehicleSearchPane createSearchPane() {
		return new BikeSearchPane();
	}

	public void onEdit(ADataSinglePane<Vehicle> singlePane, IDataManageController<Vehicle> iDataManageController ) {
		if(singlePane instanceof VehicleSinglePane) {
			Vehicle bike = singlePane.getData();
			
			new BikeEditDialog(bike, iDataManageController);
		}
			
		
	}



}

