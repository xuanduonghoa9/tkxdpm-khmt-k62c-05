package com.ebr.subsystem;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.vehicle.Vehicle;

public class ADataGetVehiclesApi implements IDataGetVehiclesApi<Vehicle>, IDataUpdateVehicleApi<Vehicle>{
	private String path;
	private static Client client;
	public static Client getClient() {
		return client;
	}

	public static void setClient(Client client) {
		ADataGetVehiclesApi.client = client;
	}
//	private static ADataGetVehiclesApi instance;
	public ADataGetVehiclesApi() {
		client = ClientBuilder.newClient();
		
	}
	

	
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public ArrayList<Vehicle> getVehicles(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path(this.path);
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Vehicle> res = response.readEntity(new GenericType<ArrayList<Vehicle>>() {});
		System.out.println(res);
		return res;
	}

	@Override
	public ArrayList<Vehicle> getAllVehicles() {
		WebTarget webTarget = client.target(PATH).path("vehicles");
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Vehicle> res = response.readEntity(new GenericType<ArrayList<Vehicle>>(){});
		System.out.println(res);
		return res;
		
	}
	@Override
	public Vehicle update(Vehicle vehicle) {
		WebTarget webTarget = client.target(PATH).path(this.path).path(vehicle.getId());
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(vehicle, MediaType.APPLICATION_JSON));
		return response.readEntity(vehicle.getClass());
	}


}
