package com.ebr.subsystem;


public interface IDataUpdateVehicleApi<T> {
	public T update( T t);
}
