package com.ebr.subsystem;

import com.ebr.bean.*;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Map;

public class AccountApi {
    public static final String PATH = "http://localhost:8080/";

    private Client client;
    private static AccountApi vehicleApi;

    public static AccountApi getInstance() {
        if (vehicleApi == null) {
            vehicleApi = new AccountApi();
        }
        return vehicleApi;
    }

    private AccountApi() {
        client = ClientBuilder.newClient();
    }


    public Account getBalance(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("account/minus");


        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }


        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
//        ArrayList<Account> res = response.readEntity(new GenericType<ArrayList<Account>>() {
//        });
        ArrayList<Account> res = response.readEntity(new GenericType<ArrayList<Account>>() {
        });
        System.out.println(res);
        if (res.size() > 0)
            return res.get(0);
        else return null;
    }
}