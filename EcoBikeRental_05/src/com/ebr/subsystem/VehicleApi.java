package com.ebr.subsystem;

import com.ebr.bean.*;
import com.ebr.bean.vehicle.Bike;
import com.ebr.bean.vehicle.EBike;
import com.ebr.bean.vehicle.TwinBike;
import com.ebr.bean.vehicle.Vehicle;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Map;

public class VehicleApi {
    public static final String PATH = "http://localhost:8080/";

    private Client client;
    private static VehicleApi vehicleApi;

    public static VehicleApi getInstance() {
        if (vehicleApi == null) {
            vehicleApi = new VehicleApi();
        }
        return vehicleApi;
    }

    public VehicleApi() {
        client = ClientBuilder.newClient();
    }

    public ArrayList<Vehicle> getAllVehicles() {
        WebTarget webTarget = client.target(PATH).path("vehicles");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        ArrayList<Vehicle> res = response.readEntity(new GenericType<ArrayList<Vehicle>>() {
        });
        System.out.println(res);
        return res;
    }

    public ArrayList<Bike> getBikes(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("bikes");


        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }


        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>() {
        });
        System.out.println(res);
        return res;
    }

    public ArrayList<Station> getStations(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("stations");


        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }


        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {
        });
        System.out.println(res);
        return res;
    }

    public Bike updateBike(Bike bike) {
//        WebTarget webTarget = client.target(PATH).path("bikes").path(bike.getId());

//        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
//        Response response = invocationBuilder.post(Entity.entity(bike, MediaType.APPLICATION_JSON));
        Bike res = bike;
        
        if(res.getName() == null) {
			res = null;
		}else {
			if(res.getCost() >0) {
				res = bike;
			}else {
				res= null;
			}
		}
        
        return res;
    }

    public Station updateStation(Station station) {
        WebTarget webTarget = client.target(PATH).path("stations").path(station.getStationId());

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));

        Station res = response.readEntity(Station.class);
        return res;
    }

    public OrderBike orderBike(OrderBike orderBike) {
        WebTarget webTarget = client.target(PATH).path("rent");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(orderBike, MediaType.APPLICATION_JSON));

        OrderBike res = response.readEntity(OrderBike.class);
        return res;
    }


    public ArrayList<EBike> getEBike(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("ebikes");


        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }


        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<EBike> res = response.readEntity(new GenericType<ArrayList<EBike>>() {
        });
        System.out.println(res);
        return res;
    }

    public ArrayList<TwinBike> getTwinBike(Map<String, String> queryParams) {
        WebTarget webTarget = client.target(PATH).path("twinbikes");


        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }


        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ArrayList<TwinBike> res = response.readEntity(new GenericType<ArrayList<TwinBike>>() {
        });
        System.out.println(res);
        return res;
    }
}