package com.ebr.subsystem;

import java.util.ArrayList;
import java.util.Map;

public interface IDataGetVehiclesApi<T> {
	public static final String PATH = "http://localhost:8080/";
	public ArrayList<T> getVehicles(Map<String, String> queryParams);
	public ArrayList<T>  getAllVehicles();
}
