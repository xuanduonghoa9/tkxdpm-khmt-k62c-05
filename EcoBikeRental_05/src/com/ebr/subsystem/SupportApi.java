package com.ebr.subsystem;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import com.ebr.bean.Station;

public class SupportApi extends ADataGetVehiclesApi{
	private static SupportApi instance;
	private SupportApi() {
		super();
		
	}
	
	public static SupportApi getInstance() {
		if (instance == null) {
			synchronized (SupportApi.class) {
				if (instance == null) {
					instance = new SupportApi();
				}
			}
		}
		return instance;
	}
	
	public ArrayList<Station> getStations(Map<String, String> queryParams) {
		WebTarget webTarget = this.getClient().target(PATH).path("stations");

		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}


		Invocation.Builder invocationBuilder = webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {});
		System.out.println(res);
		return res;
	}
	
	 public Station updateStation(Station station) {
	        WebTarget webTarget = this.getClient().target(PATH).path("stations").path(station.getStationId());

	        Invocation.Builder invocationBuilder = webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON);
	        Response response = invocationBuilder.post(Entity.entity(station, javax.ws.rs.core.MediaType.APPLICATION_JSON));

	        Station res = response.readEntity(Station.class);
	        return res;
	    }


}
