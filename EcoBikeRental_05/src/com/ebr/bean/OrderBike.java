package com.ebr.bean;

import java.util.Date;

public class OrderBike {
    private String rentalBikeID;
    private String bikeID;
    private Date checkin;
    private Date checkout;
    private String returnBike;
    private String customerName;
    private String customerCard;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCard() {
        return customerCard;
    }

    public void setCustomerCard(String customerCard) {
        this.customerCard = customerCard;
    }

    public String getRentalBikeID() {
        return rentalBikeID;
    }

    public void setRentalBikeID(String rentalBikeID) {
        this.rentalBikeID = rentalBikeID;
    }

    public String getBikeID() {
        return bikeID;
    }

    public void setBikeID(String bikeID) {
        this.bikeID = bikeID;
    }

    public Date getCheckin() {
        return checkin;
    }

    public void setCheckin(Date checkin) {
        this.checkin = checkin;
    }

    public Date getCheckout() {
        return checkout;
    }

    public void setCheckout(Date checkout) {
        this.checkout = checkout;
    }

    public String getReturnBike() {
        return returnBike;
    }

    public void setReturnBike(String returnBike) {
        this.returnBike = returnBike;
    }
}
