package com.ebr.bean.vehicle;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = Bike.class, name = "bike"), @JsonSubTypes.Type(value = EBike.class, name = "ebike"), @JsonSubTypes.Type(value = TwinBike.class, name = "twinbike")})
public class BikeVehicle extends Vehicle {
    private String type;
    
	public BikeVehicle() {
		super();
	}
    
    public BikeVehicle(String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost, String type, String stationId) {
    	 super(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost, stationId);
    }

    public BikeVehicle(String type, String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost, String stationId) {
        super(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost, stationId);
        this.type = type;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", type='" + type + '\'';
    }

    @Override
    public boolean match(Vehicle vehicle) {
        if (vehicle == null)
            return true;


        boolean res = super.match(vehicle);
        if (!res) {
            return false;
        }


        if (!(vehicle instanceof Vehicle))
            return false;
        BikeVehicle bikeVehicle = (BikeVehicle) vehicle;


        if (bikeVehicle.type != null && !bikeVehicle.type.equals("") && !this.type.contains(bikeVehicle.type)) {
            return false;
        }
        return true;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
