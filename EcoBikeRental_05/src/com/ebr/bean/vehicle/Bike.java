package com.ebr.bean.vehicle;

public class Bike extends BikeVehicle{

//    public Bike() {
//        
//    }
	
	public Bike() {
		super();
	}

    public Bike(String type, String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost,  String stationId) {
        super(type, id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost, stationId);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public boolean match(Vehicle vehicle) {
        return super.match(vehicle);
    }
}
