package com.ebr.bean.vehicle;

public class TwinBike extends BikeVehicle {

//    public TwinBike(String type) {
//        super(type);
//    }

    public TwinBike(String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost, String type, String stationId) {
        super(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost, type, stationId);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public boolean match(Vehicle vehicle) {
        return super.match(vehicle);
    }
}
