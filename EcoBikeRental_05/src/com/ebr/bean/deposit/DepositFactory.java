package com.ebr.bean.deposit;

public class DepositFactory {
    private static DepositFactory depositFactory;
    public static DepositFactory getInstance(){
        if (depositFactory == null){
            return new DepositFactory();
        }
        return depositFactory;
    }

    public VehicleDeposit getDepositVehicle(String type){
        if (type.equals("bike")){
            return new BikeDeposit();
        }
        if (type.equals("ebike")){
            return new EBikeDeposit();
        }
        if (type.equals("twinbike")){
            return new TwinBikeDeposit();
        }
        return null;
    }
}
