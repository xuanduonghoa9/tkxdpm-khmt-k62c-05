package com.ebr.bean.deposit;

public class TwinBikeDeposit extends VehicleDeposit {
    public TwinBikeDeposit() {
    }

    @Override
    public float getDepositMoney() {
        return 550000f;
    }
}