package com.ebr.bean.deposit;

public class BikeDeposit extends VehicleDeposit {
    public BikeDeposit() {
    }

    @Override
    public float getDepositMoney() {
        return 400000f;
    }
}
