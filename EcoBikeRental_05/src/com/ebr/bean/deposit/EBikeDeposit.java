package com.ebr.bean.deposit;

public class EBikeDeposit extends VehicleDeposit {
    public EBikeDeposit() {
    }

    @Override
    public float getDepositMoney() {
        return 700000f;
    }
}