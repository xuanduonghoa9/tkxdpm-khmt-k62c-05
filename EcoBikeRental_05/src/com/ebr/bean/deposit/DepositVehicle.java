package com.ebr.bean.deposit;

public interface DepositVehicle {
    public float getDepositMoney();
}
