package com.ebr.bean;

public class RentalBike {
    private String id;
    private String barcode;
    private String customerName;
    private String customerCard;
    private Double totalCost;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RentalBike) {
            return this.barcode.equals(((RentalBike) obj).barcode);
        }
        return false;
    }

    public boolean search(RentalBike rentalBike) {
        if (this.id != null && !this.id.equals("") && !this.id.contains(rentalBike.id)) {
            return false;
        }
        if (this.barcode != null && !this.barcode.equals("") && !this.barcode.contains(rentalBike.barcode)) {
            return false;
        }
        if (this.customerName != null && !this.customerName.equals("") && !this.customerName.contains(rentalBike.customerName)) {
            return false;
        }
        if (this.customerCard != null && !this.customerCard.equals("") && !this.customerCard.contains(rentalBike.customerCard)) {
            return false;
        }
        if (this.totalCost != 0 && this.totalCost != rentalBike.totalCost) {
            return false;
        }
        return true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCard() {
        return customerCard;
    }

    public void setCustomerCard(String customerCard) {
        this.customerCard = customerCard;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }
}
