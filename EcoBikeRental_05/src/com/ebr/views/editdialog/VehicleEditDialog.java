package com.ebr.views.editdialog;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ebr.bean.vehicle.Vehicle;
import com.ebr.controller.IDataManageController;

@SuppressWarnings("serial")
public class VehicleEditDialog extends ADataEditDialog<Vehicle>{
	
	private JTextField barcodeField;
	private JTextField nameField;
	private JTextField weightField;
	private JTextField licensePlateField;
	private JTextField manufacturingDateField;
	private JTextField producerField;
	private JTextField costField;
	
	public VehicleEditDialog(Vehicle vehicle, IDataManageController<Vehicle> controller) {
		super(vehicle, controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel barcodeLabel = new JLabel("Barcode");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(barcodeLabel, c);
		barcodeField = new JTextField(15);
		barcodeField.setText(t.getBarcode());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(barcodeField, c);
		
		
		row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Name");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		nameField.setText(t.getName());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);
		
		row = getLastRowIndex();
		JLabel weightLabel = new JLabel("Weight");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(weightLabel, c);
		weightField = new JTextField(15);
		weightField.setText(t.getWeight() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(weightField, c);
		
		row = getLastRowIndex();
		JLabel licensePlateLabel = new JLabel("License Plate");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(licensePlateLabel, c);
		licensePlateField = new JTextField(15);
		licensePlateField.setText(t.getLicensePlate() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(licensePlateField, c);

		row = getLastRowIndex();
		JLabel manufacturingDateLabel = new JLabel("Manufacturing Date");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(manufacturingDateLabel, c);
		manufacturingDateField = new JTextField(15);
		manufacturingDateField.setText(t.getManufacturingDate() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(manufacturingDateField, c);
		
		row = getLastRowIndex();
		JLabel producerLabel = new JLabel("Producer");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(producerLabel, c);
		producerField = new JTextField(15);
		producerField.setText(t.getProducer() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(producerField, c);

		row = getLastRowIndex();
		JLabel costLabel = new JLabel("Cost");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(costLabel, c);
		costField = new JTextField(15);
		costField.setText(t.getCost() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(costField, c);
		
	
	}

	@Override
	public Vehicle getNewData() {
		t.setBarcode(barcodeField.getText());
		t.setName(nameField.getText());
		t.setWeight(Float.parseFloat(weightField.getText()));
		t.setLicensePlate(licensePlateField.getText());
		t.setManufacturingDate(manufacturingDateField.getText());
		t.setProducer(producerField.getText());
		t.setCost(Double.parseDouble(costField.getText()));
		return t;
	}
}
