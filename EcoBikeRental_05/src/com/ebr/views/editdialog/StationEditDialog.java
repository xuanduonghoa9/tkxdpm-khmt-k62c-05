package com.ebr.views.editdialog;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ebr.bean.Station;
import com.ebr.controller.IDataManageController;

@SuppressWarnings("serial")
public class StationEditDialog extends ADataEditDialog<Station>{
	private JTextField stationNameField;
	private JTextField stationAddressField;
	private JTextField numberOfBikesField;
	private JTextField numberOfEBikesField;
	private JTextField numberTwinBikesField;
	private JTextField numberOfEmptyField;
	private JTextField numberOfDocks;
	
	public StationEditDialog(Station station, IDataManageController<Station> controller) {
		super(station, controller);
	}

	@Override
	public void buildControls() {
		if (t instanceof Station) {
			Station station = (Station) t;
			
			int row = getLastRowIndex();
			JLabel stationNameLabel = new JLabel("Station name");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(stationNameLabel, c);
			stationNameField = new JTextField(15);
			stationNameField.setText(station.getStationName());
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(stationNameField, c);
			
			
			row = getLastRowIndex();
			JLabel stationAddressLabel = new JLabel("Station Address");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(stationAddressLabel, c);
			stationAddressField = new JTextField(15);
			stationAddressField.setText(station.getStationAddress());
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(stationAddressField, c);
			
			
			row = getLastRowIndex();
			JLabel numBikesLabel = new JLabel("Number Of Bikes");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(numBikesLabel, c);
			numberOfBikesField = new JTextField(15);
			numberOfBikesField.setText(station.getNumberOfBikes() + "");
			numberOfBikesField.setEditable(false);
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(numberOfBikesField, c);
			
			
			row = getLastRowIndex();
			JLabel numEBikesLabel = new JLabel("Number Of EBikes");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(numEBikesLabel, c);
			numberOfEBikesField = new JTextField(15);
			numberOfEBikesField.setText(station.getNumberOfEBikes() + "");
			numberOfEBikesField.setEditable(false);
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(numberOfEBikesField, c);
			
			
			row = getLastRowIndex();
			JLabel numTwinBikesLabel = new JLabel("Number Of Twin Bikes");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(numTwinBikesLabel, c);
			numberTwinBikesField = new JTextField(15);
			numberTwinBikesField.setText(station.getNumberOfTwinBikes() + "");
			numberTwinBikesField.setEditable(false);
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(numberTwinBikesField, c);
			
			
			row = getLastRowIndex();
			JLabel numEmtyLabel = new JLabel("Number Of Empty");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(numEmtyLabel, c);
			numberOfEmptyField = new JTextField(15);
			numberOfEmptyField.setText(station.getNumberOfEmpty() + "");
			numberOfEmptyField.setEditable(false);
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(numberOfEmptyField, c);
			
			
			row = getLastRowIndex();
			JLabel numDocksLabel = new JLabel("Number Of Docks");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(numDocksLabel, c);
			numberOfDocks = new JTextField(15);
			numberOfDocks.setText(station.getNumberOfDocks() + "");
			numberOfDocks.setEditable(false);
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(numberOfDocks, c);
		}
	}

	@Override
	public Station getNewData() {
		if (t instanceof Station) {
			Station station =  t;
			station.setStationName(stationNameField.getText());
			station.setStationAddress(stationAddressField.getText());
			station.setNumberOfBikes(Integer.parseInt(numberOfBikesField.getText()));
			station.setNumberOfEBikes(Integer.parseInt(numberOfEBikesField.getText()));
			station.setNumberOfTwinBikes(Integer.parseInt(numberTwinBikesField.getText()));
			station.setNumberOfEmpty(Integer.parseInt(numberOfEmptyField.getText()));
			station.setNumberOfDocks(Integer.parseInt(numberOfDocks.getText()));
		}
		
		return t;
	}
}
