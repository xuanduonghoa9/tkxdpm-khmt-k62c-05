package com.ebr.views.listPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import com.ebr.bean.Station;
import com.ebr.controller.IDataManageController;
import com.ebr.controller.user.UserStationPageController;
import com.ebr.views.singlePane.ADataSinglePane;
import com.ebr.views.singlePane.StationSinglePane;

public class UserStationListPane extends ADataListPane<Station>{
	private IDataManageController<Station> iDataManageController;

	public UserStationListPane(UserStationPageController controller) {
		this.controller = controller;
	}
	@Override
	public void decorateSinglePane(ADataSinglePane<Station> singlePane) {

		
		JButton button = new JButton("Xem danh sách xe trong bãi xe");
		singlePane.addDataHandlingComponent(button);
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (controller instanceof UserStationPageController) {
					if(singlePane instanceof StationSinglePane)
						((UserStationPageController) controller)
								.viewVehicles(((StationSinglePane) singlePane).getData(), iDataManageController);
				}
			}
		});
		
	}

}
