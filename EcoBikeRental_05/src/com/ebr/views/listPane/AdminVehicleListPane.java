package com.ebr.views.listPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.ebr.bean.vehicle.Bike;
import com.ebr.bean.vehicle.Vehicle;
import com.ebr.controller.ADataPageController;
import com.ebr.controller.IDataManageController;
import com.ebr.controller.admin.AdminBikePageController;
import com.ebr.subsystem.VehicleApi;
import com.ebr.views.singlePane.ADataSinglePane;

//@authors PHUNG HA DUONG_ 20173061

@SuppressWarnings("serial")
public class AdminVehicleListPane extends ADataListPane<Vehicle>{
	private IDataManageController<Vehicle> iDataManageController;
	public AdminVehicleListPane(ADataPageController<Vehicle> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<Vehicle> singlePane) {
		JButton button = new JButton("Edit");
		singlePane.addDataHandlingComponent(button);
	
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (controller instanceof AdminBikePageController) {
					((AdminBikePageController) controller).onEdit(singlePane,  new IDataManageController<Vehicle>() {


						@Override
						public void onAct(Vehicle t) {
							Bike bike = (Bike) t;
							VehicleApi mediaApi = VehicleApi.getInstance();				
							mediaApi.updateBike(bike);
							singlePane.updateData(bike);
							
						}
						
					});
				}
			}
		});
	}
	public void setController(IDataManageController<Vehicle>  iDataManageController) {
		this.iDataManageController = iDataManageController;
	}

}
