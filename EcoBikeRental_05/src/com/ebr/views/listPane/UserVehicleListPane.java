package com.ebr.views.listPane;

import com.ebr.bean.OrderBike;
import com.ebr.bean.vehicle.Vehicle;
import com.ebr.controller.ADataPageController;
import com.ebr.controller.IDataManageController;
import com.ebr.controller.user.UserBikePageController;
import com.ebr.views.singlePane.ADataSinglePane;
import com.ebr.views.singlePane.vehicle.BikeSinglePane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserVehicleListPane extends ADataListPane<Vehicle> {
    public UserVehicleListPane(ADataPageController<Vehicle> controller) {
        this.controller = controller;
    }
    @Override
    public void decorateSinglePane(ADataSinglePane<Vehicle> singlePane) {
        JButton buttonRental = new JButton("Thuê xe");
        singlePane.addDataHandlingComponent(buttonRental);
        buttonRental.setBackground(Color.MAGENTA);
        buttonRental.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller instanceof UserBikePageController) {
                    OrderBike orderBike = new OrderBike();
                    IDataManageController<OrderBike> iDataManageController = new IDataManageController<OrderBike>() {
                

                        @Override
                        public void onAct(OrderBike orderBike) {

                        }
                    };

//                    setController(rentalController);
                    if (singlePane instanceof BikeSinglePane) {
                        ((UserBikePageController) controller)
                                .rentBike(((BikeSinglePane) singlePane).getData(), orderBike, iDataManageController);
                    }

                }
            }
        });
    }
}
