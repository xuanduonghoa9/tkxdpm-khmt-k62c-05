package com.ebr.views.listPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.ebr.bean.Station;
import com.ebr.controller.ADataPageController;
import com.ebr.controller.IDataManageController;
import com.ebr.controller.admin.AdminStationPageController;
import com.ebr.views.singlePane.ADataSinglePane;
import com.ebr.views.singlePane.StationSinglePane;

//@authors PHUNG HA DUONG_ 20173061

@SuppressWarnings("serial")
public class AdminStationListPane extends ADataListPane<Station> {
	private IDataManageController<Station> iDataManageController;

	public AdminStationListPane(ADataPageController<Station> controller) {
		this.controller = controller;
	}

	@Override
	public void decorateSinglePane(ADataSinglePane<Station> singlePane) {
		JButton buttonEdit = new JButton("Edit");
		JButton buttonViewBikeAdmin = new JButton("Xem thông tin xe trong bãi");

		singlePane.addDataHandlingComponent(buttonEdit);
		singlePane.addDataHandlingComponent(buttonViewBikeAdmin);
		buttonEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (controller instanceof AdminStationPageController) {
					((AdminStationPageController) controller).onEdit(singlePane,  iDataManageController);
				}
			}
		});
		
		buttonViewBikeAdmin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (controller instanceof AdminStationPageController) {
					if (singlePane instanceof StationSinglePane) {
						((AdminStationPageController) controller)
								.viewAdminVehicles(((StationSinglePane) singlePane).getData(), iDataManageController);
					}
				} 
			}
		});
	}

	public void setController(IDataManageController<Station> iDataManageController) {
		this.iDataManageController = iDataManageController;
	}

}
