package com.ebr.views.singlePane;

import javax.swing.JLabel;

import com.ebr.bean.Station;

@SuppressWarnings("serial")
public class StationSinglePane extends ADataSinglePane<Station>{
	private JLabel labelStationName;
	private JLabel labelStationAddress;
	private JLabel labelNumberOfBikes;
	private JLabel labelNumberOfEBikes;
	private JLabel labelNumberOfTwinBikes;
	private JLabel labelNumberOfEmpty;
	private JLabel labelNumberOfDocks;
	

	public StationSinglePane() {
		super();
	}
		
	
	public StationSinglePane(Station station) {
		this();
		this.t = station;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStationName = new JLabel();
		add(labelStationName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStationAddress = new JLabel();
		add(labelStationAddress, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfBikes = new JLabel();
		add(labelNumberOfBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfEBikes = new JLabel();
		add(labelNumberOfEBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfTwinBikes = new JLabel();
		add(labelNumberOfTwinBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfEmpty = new JLabel();
		add(labelNumberOfEmpty, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfDocks = new JLabel();
		add(labelNumberOfDocks, c);
	}
	
	
	@Override
	public void displayData() {
		labelStationName.setText("Name: " + t.getStationName());
		labelStationAddress.setText("Address: " + t.getStationAddress());
		labelNumberOfBikes.setText("Number Of Bikes: " + t.getNumberOfBikes()+ "");
		labelNumberOfEBikes.setText("Number Of EBikes: " + t.getNumberOfEBikes()+"");
		labelNumberOfTwinBikes.setText("Number Of Twin Bikes: " + t.getNumberOfTwinBikes()+"");
		labelNumberOfEmpty.setText("Number Of Empty: " + t.getNumberOfEmpty()+"");
		labelNumberOfDocks.setText("Number Of Docks: " + t.getNumberOfDocks() + "");
	}

}
