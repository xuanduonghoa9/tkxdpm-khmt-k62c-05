package com.ebr.views.singlePane.vehicle;

import com.ebr.bean.vehicle.Vehicle;

@SuppressWarnings("serial")
public class BikeVehicleSinglePane extends VehicleSinglePane{
	
	public BikeVehicleSinglePane() {
		super();
	}

	public BikeVehicleSinglePane(Vehicle vehicle) {
		this();
		this.t = vehicle;
		
		displayData();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();	
	}
	
	@Override
	public void displayData() {
		super.displayData();		

	}
}
