package com.ebr.views.singlePane.vehicle;

import javax.swing.JLabel;

import com.ebr.bean.vehicle.Vehicle;
import com.ebr.views.singlePane.ADataSinglePane;

@SuppressWarnings("serial")
public class VehicleSinglePane extends ADataSinglePane<Vehicle> {
	private JLabel labelBarcode;
	private JLabel labelName;
	private JLabel labelWeight;
	private JLabel labelLicensePlate;
	private JLabel labelManufacturingDate;
	private JLabel labelProducer;
	private JLabel labelCost;
	private JLabel labelStation;
	

	public VehicleSinglePane() {
		super();
	}
		
	
	public VehicleSinglePane(Vehicle vehicle) {
		this();
		this.t = vehicle;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelBarcode = new JLabel();
		add(labelBarcode, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelWeight = new JLabel();
		add(labelWeight, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelLicensePlate = new JLabel();
		add(labelLicensePlate, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelManufacturingDate = new JLabel();
		add(labelManufacturingDate, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelProducer = new JLabel();
		add(labelProducer, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelCost = new JLabel();
		add(labelCost, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStation = new JLabel();
		add(labelStation, c);
	}
	
	
	@Override
	public void displayData() {
		labelBarcode.setText("Barcode: " + t.getBarcode());
		labelName.setText("Name: " + t.getName());
		labelWeight.setText("Weight: " + t.getWeight());
		labelLicensePlate.setText("License Plate: " + t.getLicensePlate());
		labelManufacturingDate.setText("Manufacturing Date: " + t.getManufacturingDate());
		labelProducer.setText("Producer: " + t.getProducer());
		labelCost.setText("Cost: " + t.getCost() + "");
		labelStation.setText("Station: " + t.getStationId());

	}
}
