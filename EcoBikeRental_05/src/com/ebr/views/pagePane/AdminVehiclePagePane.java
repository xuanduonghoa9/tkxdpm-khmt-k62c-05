package com.ebr.views.pagePane;

import java.awt.BorderLayout;

import javax.swing.*;
import java.awt.*;

import com.ebr.controller.admin.*;;

public class AdminVehiclePagePane extends JFrame{
	 public static final int WINDOW_WIDTH = 800;
	    public static final int WINDOW_HEIGHT = 550;
	    public AdminVehiclePagePane(){
	        super();
	    }
	    public void getViewListVehicle(String station){
	        JPanel rootPanel = new JPanel();
	        setContentPane(rootPanel);
	        BorderLayout layout = new BorderLayout();
	        rootPanel.setLayout(layout);
	        JTabbedPane tabbedPane = new JTabbedPane();
	        rootPanel.add(tabbedPane, BorderLayout.CENTER);
	        AdminBikePageController controller = new AdminBikePageController(station);
	        JPanel bike = controller.viewBike(station);
	        tabbedPane.addTab("Bike", null, bike, "Bike");
	        tabbedPane.addTab("EBike", null, new JPanel(), "EBike");
	        tabbedPane.addTab("TwinBike", null, new JPanel(), "TwinBike");
//	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        setTitle("Xem thông tin trong bãi xe");
	        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	        setVisible(true);
	    }
}
