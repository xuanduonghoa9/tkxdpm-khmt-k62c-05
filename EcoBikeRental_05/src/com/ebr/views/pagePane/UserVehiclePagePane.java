package com.ebr.views.pagePane;

//<<<<<<< HEAD
//<<<<<<< HEAD
import com.ebr.controller.user.UserBikePageController;
//=======
//import com.ebr.controller.user.UserBikePageController;
//=======
//>>>>>>> 046db4f0088567a2390243e142e8478416f23282
import com.ebr.controller.user.UserVehiclePageController;
import com.ebr.factory.UserPageFactory;
import javax.swing.*;
import java.awt.*;

public class UserVehiclePagePane extends JFrame {
    public static final int WINDOW_WIDTH = 800;
    public static final int WINDOW_HEIGHT = 550;
    public UserVehiclePagePane(){
        super();
    }
    public void getViewListVehicle(String station){
        JPanel rootPanel = new JPanel();
        setContentPane(rootPanel);
        BorderLayout layout = new BorderLayout();
        rootPanel.setLayout(layout);
        JTabbedPane tabbedPane = new JTabbedPane();
        rootPanel.add(tabbedPane, BorderLayout.CENTER);
        UserVehiclePageController controller = UserPageFactory.getInstance().createPage("UserBikePage", station);

        JPanel bike = controller.viewBike(station);
        rootPanel.add(controller.getRentalPane(), BorderLayout.NORTH);
        tabbedPane.addTab("Bike", null, bike, "Bike");
        tabbedPane.addTab("EBike", null, new JPanel(), "EBike");
        tabbedPane.addTab("TwinBike", null, new JPanel(), "TwinBike");
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Xem thông tin trong bãi xe");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setVisible(true);
    }

}
