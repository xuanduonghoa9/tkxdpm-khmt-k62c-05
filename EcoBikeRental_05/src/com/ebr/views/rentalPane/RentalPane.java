package com.ebr.views.rentalPane;

import com.ebr.controller.user.RentalController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RentalPane extends JPanel {
    private JLabel rentalStatusLabel;
    private RentalController controller;

    public RentalPane() {
        this.setLayout(new FlowLayout(FlowLayout.RIGHT));
        rentalStatusLabel = new JLabel();
        this.add(rentalStatusLabel);
        JButton detailButton = new JButton("Chi tiết");
        this.add(detailButton);


        detailButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateData("HXDHXD");
            }
        });
    }

    public void setController(RentalController controller) {
        this.controller = controller;
    }

    public void updateData(String text) {
        rentalStatusLabel.setText(text);
    }
}
