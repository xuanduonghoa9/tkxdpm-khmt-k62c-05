package com.ebr.views.search;

import java.util.Map;


@SuppressWarnings("serial")
public class BikeSearchPane extends BikeVehicleSearchPane {

	public BikeSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
		
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		return res;
	}
}
