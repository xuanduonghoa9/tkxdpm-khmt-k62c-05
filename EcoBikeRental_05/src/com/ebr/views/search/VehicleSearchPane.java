package com.ebr.views.search;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class VehicleSearchPane extends ADataSearchPane {
	private JTextField barcodeField;
	private JTextField nameField;
	private JTextField licensePlateField;
	private JTextField producerField;

	public VehicleSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel barcodeLabel = new JLabel("Barcode");
		barcodeField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(barcodeLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(barcodeField, c);
		
		JLabel nameLabel = new JLabel("Name");
		nameField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(nameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);
		
		JLabel licnesePlateLabel = new JLabel("License Plate ");
		licensePlateField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(licnesePlateLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(licensePlateField, c);
		
		JLabel producerLabel = new JLabel("Producer");
		producerField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(producerLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(producerField, c);
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!barcodeField.getText().trim().equals("")) {
			res.put("barcode", barcodeField.getText().trim());
		}
		if (!nameField.getText().trim().equals("")) {
			res.put("name", nameField.getText().trim());
		}
		if (!licensePlateField.getText().trim().equals("")) {
			res.put("license plate", licensePlateField.getText().trim());
		}
		if (!producerField.getText().trim().equals("")) {
			res.put("producer", producerField.getText().trim());
		}
		return res;
	}
}
