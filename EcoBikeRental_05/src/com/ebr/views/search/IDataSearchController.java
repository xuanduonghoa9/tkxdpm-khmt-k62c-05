package com.ebr.views.search;

import java.util.Map;

public interface IDataSearchController {
	public void search(Map<String, String> searchParams);
}
