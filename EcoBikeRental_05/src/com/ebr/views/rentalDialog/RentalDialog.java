package com.ebr.views.rentalDialog;

import com.ebr.bean.*;
import com.ebr.bean.vehicle.Bike;
import com.ebr.bean.vehicle.EBike;
import com.ebr.bean.vehicle.TwinBike;
import com.ebr.bean.vehicle.Vehicle;
import com.ebr.controller.IDataManageController;
import com.ebr.controller.user.RentalController;
import com.ebr.bean.deposit.DepositFactory;

import javax.swing.*;
import java.sql.Timestamp;

public class RentalDialog extends ADataRentalDialog<Vehicle, OrderBike> {
    private JTextField bikeId;
    private JTextField customerName;
    private JTextField customerCardId;
    private JPasswordField customerCardPassword;
    private JTextField moneyDeposit;
    private RentalController rentalController;

    public RentalDialog(Vehicle vehicle, OrderBike orderBike, IDataManageController<OrderBike> controller, RentalController rentalController) {
        super(vehicle, orderBike, controller, rentalController);
    }

    public RentalController getRentalController() {
        return rentalController;
    }

    public void setRentalController(RentalController rentalController) {
        this.rentalController = rentalController;
    }

    @Override
    public void buildControls() {
        if (t != null) {
            Vehicle vehicle = (Vehicle) t;

            int row = getLastRowIndex();
            JLabel vehicleNameLabel = new JLabel("Mã xe thuê");
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(vehicleNameLabel, c);
            bikeId = new JTextField(15);
            bikeId.setText(vehicle.getId());
            c.gridx = 1;
            c.gridy = row;
            bikeId.setEditable(false);
            getContentPane().add(bikeId, c);


            row = getLastRowIndex();
            JLabel moneyLabel = new JLabel("Tiền đặt cọc");
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(moneyLabel, c);
            moneyDeposit = new JTextField(15);
            c.gridx = 1;
            c.gridy = row;
            if (vehicle instanceof Bike) {
                moneyDeposit.setText(String.valueOf(DepositFactory.getInstance().getDepositVehicle("bike").getDepositMoney()));
            } else if (vehicle instanceof EBike) {
                moneyDeposit.setText(String.valueOf(DepositFactory.getInstance().getDepositVehicle("ebike").getDepositMoney()));
            } else if (vehicle instanceof TwinBike) {
                moneyDeposit.setText(String.valueOf(DepositFactory.getInstance().getDepositVehicle("twinbike").getDepositMoney()));
            }
            moneyDeposit.setEditable(false);
            getContentPane().add(moneyDeposit, c);

            row = getLastRowIndex();
            JLabel vehicleAddressLabel = new JLabel("Tên khách hàng");
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(vehicleAddressLabel, c);
            customerName = new JTextField(15);
            c.gridx = 1;
            c.gridy = row;
            getContentPane().add(customerName, c);

            row = getLastRowIndex();
            JLabel cardIdLabel = new JLabel("Mã thẻ tín dụng");
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(cardIdLabel, c);
            customerCardId = new JTextField(15);
            c.gridx = 1;
            c.gridy = row;
            getContentPane().add(customerCardId, c);

            row = getLastRowIndex();
            JLabel pwLabel = new JLabel("Mật khẩu");
            c.gridx = 0;
            c.gridy = row;
            getContentPane().add(pwLabel, c);
            customerCardPassword = new JPasswordField(15);
            c.gridx = 1;
            c.gridy = row;
            getContentPane().add(customerCardPassword, c);
        }
    }

    @Override
    public OrderBike getNewData() {
        if (o instanceof OrderBike) {
            OrderBike orderBike = new OrderBike();
            orderBike.setBikeID(bikeId.getText());
            orderBike.setCustomerCard(customerCardId.getText());
            orderBike.setCustomerName(customerName.getText());
            orderBike.setCheckin(new Timestamp(System.currentTimeMillis()));
            orderBike.setCheckout(null);
            return orderBike;
        } else return null;
    }

    @Override
    public boolean deposit(RentalController rentalController) {
        String cardId = customerCardId.getText();
        String password = customerCardPassword.getText();
        float money = Float.parseFloat(moneyDeposit.getText());
        return rentalController.checkBalance(cardId, password, money);
    }
}
