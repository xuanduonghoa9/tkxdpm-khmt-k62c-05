package com.ebr.views.rentalDialog;

import com.ebr.controller.IDataManageController;
import com.ebr.controller.user.RentalController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public abstract class ADataRentalDialog<T, O> extends JDialog {
    protected T t;
    protected O o;
    protected RentalController rentalController;
    protected GridBagLayout layout;
    protected GridBagConstraints c = new GridBagConstraints();

    public ADataRentalDialog(T t, O o, IDataManageController<O> controller, RentalController rentalController) {
        super((Frame) null, "Thuê xe", true);

        this.t = t;
        this.o = o;

        setContentPane(new JPanel());
        layout = new GridBagLayout();
        getContentPane().setLayout(layout);


        this.buildControls();

        JButton saveButton = new JButton("Save");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                O newO = getNewData();
                controller.onAct(newO);

                ADataRentalDialog.this.dispose();
                if (deposit(rentalController)) {
                    rentalController.updateRentalPane();
                    showMessage("Thuê xe thành công", "Thành công", "information");
                } else {
                    showMessage("Thuê xe đã xảy ra lỗi", "Lỗi", "error");
                }

            }
        });


        c.gridx = 1;
        c.gridy = getLastRowIndex();
        getContentPane().add(saveButton, c);


        this.pack();
        this.setResizable(false);
        this.setVisible(true);
    }

    protected int getLastRowIndex() {
        layout.layoutContainer(getContentPane());
        int[][] dim = layout.getLayoutDimensions();
        int rows = dim[1].length;
        return rows;
    }

    protected void showMessage(String message, String title, String type) {
        switch (type) {
            case "information":
                JOptionPane.showMessageDialog(new JPanel(), message, title,
                        JOptionPane.INFORMATION_MESSAGE);
                break;
            case "error":
                JOptionPane.showMessageDialog(new JPanel(), message, title,
                        JOptionPane.ERROR_MESSAGE);
                break;
        }
    }

    public abstract void buildControls();


    public abstract O getNewData();

    public abstract boolean deposit(RentalController rentalController);

}
