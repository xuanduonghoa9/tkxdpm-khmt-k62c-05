package com.ebr.factory;

import com.ebr.controller.user.UserBikePageController;
import com.ebr.controller.user.UserVehiclePageController;

public class UserPageFactory {
    private static UserPageFactory userPageFactory;

    private UserPageFactory() {

    }

    public static UserPageFactory getInstance() {
        if (userPageFactory == null){
            userPageFactory = new UserPageFactory();
        }
        return userPageFactory;
    }

    public UserVehiclePageController createPage(String type, String stationId){
        if (type.equals("UserBikePage")){
            return new UserBikePageController(stationId);
        }
//        else if (type.equals("UserEBikePage")){
//            return new UserBikePageController();
//        }
        return null;
    }
}
