package com.oms.db;

import com.oms.bean.Vehicle;
import com.oms.bean.RentalBike;
import com.oms.bean.Vehicle;
import com.oms.db.seed.Seed;

import java.util.ArrayList;

public class JsonVehicleDatabase implements IVehicleDatabase {
    private static IVehicleDatabase singleton = new JsonVehicleDatabase();
    private ArrayList<Vehicle> vehicles = Seed.singleton().getVehicles();
    private JsonVehicleDatabase() {
    }

    public static IVehicleDatabase singleton() {
        return singleton;
    }

    @Override
    public ArrayList<Vehicle> searchVehicle(Vehicle vehicle) {
        ArrayList<Vehicle> res = new ArrayList<Vehicle>();
        for (Vehicle b: vehicles) {
            if (b.match(vehicle)) {
                res.add(b);
            }
        }
        return res;
    }

    @Override
    public Vehicle addVehicle(Vehicle vehicle) {
        for (Vehicle m: vehicles) {
            if (m.equals(vehicle)) {
                return null;
            }
        }

        vehicles.add(vehicle);
        return vehicle;
    }

    @Override
    public Vehicle updateVehicle(Vehicle vehicle) {
        for (Vehicle m: vehicles) {
            if (m.equals(vehicle)) {
                vehicles.remove(m);
                vehicles.add(vehicle);
                return vehicle;
            }
        }
        return null;
    }

    @Override
    public ArrayList<RentalBike> searchRentalBike(RentalBike rentalBike) {
        ArrayList<RentalBike> rentalBikes = new ArrayList<RentalBike>();
        for (RentalBike rentalBike1: rentalBikes) {
            if (rentalBike1.search(rentalBike)) {
                rentalBikes.add(rentalBike1);
            }
        }
        return rentalBikes;
    }
//
//    @Override
//    public boolean checkOutRentalBike(RentalBike rentalBike) {
//        return false;
//    }

}