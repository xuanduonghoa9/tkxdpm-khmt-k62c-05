package com.oms.db;

import java.util.ArrayList;

import com.oms.bean.Station;

public interface IStationDatabase {
    public ArrayList<Station> searchStation(Station station);

    public Station updateStation(Station station);

    public Station addStation(Station station);
}
