package com.oms.db;

import com.oms.bean.RentalBike;
import com.oms.db.seed.Seed;

import java.util.ArrayList;

public class JsonRentDatabase implements IRentDatabase {
    private static IRentDatabase singleton = new JsonRentDatabase();

    private ArrayList<RentalBike> rentalBikes = Seed.singleton().getRentalBikes();

    private JsonRentDatabase() {
    }

    public static IRentDatabase singleton() {
        return singleton;
    }

    @Override
    public RentalBike searchRent(RentalBike rentalBike) {
        ArrayList<RentalBike> res = new ArrayList<RentalBike>();
        for (RentalBike b : rentalBikes) {
            if (b.match(rentalBike)) {
                res.add(b);
            }
        }
        return res.get(0);
    }
//
//    @Override
//    public RentalBike updateRent(RentalBike rentalBike) {
//        for (RentalBike m : rentalBikes) {
//            if (m.equals(rentalBike)) {
//                rentalBikes.remove(m);
//                rentalBikes.add(rentalBike);
//                return rentalBike;
//            }
//        }
//        return null;
//    }

    @Override
    public RentalBike rentVehicle(RentalBike rentalBike) {
        for (RentalBike m : rentalBikes) {
            if (m.equals(rentalBike)) {
                return null;
            }
        }

        rentalBikes.add(rentalBike);
        return rentalBike;
    }

    @Override
    public RentalBike returnVehicle(RentalBike rentalBike) {
        return null;
    }

    @Override
    public RentalBike getVehicle(RentalBike rentalBike) {
        return null;
    }
}
