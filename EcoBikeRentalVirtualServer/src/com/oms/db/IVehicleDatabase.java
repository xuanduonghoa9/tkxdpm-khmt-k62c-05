package com.oms.db;

import com.oms.bean.RentalBike;
import com.oms.bean.Vehicle;

import java.util.ArrayList;

public interface IVehicleDatabase {
    public ArrayList<Vehicle> searchVehicle(Vehicle vehicle);

    public Vehicle addVehicle(Vehicle vehicle);

    public Vehicle updateVehicle(Vehicle vehicle);

    public ArrayList<RentalBike> searchRentalBike(RentalBike rentalBike);
}
