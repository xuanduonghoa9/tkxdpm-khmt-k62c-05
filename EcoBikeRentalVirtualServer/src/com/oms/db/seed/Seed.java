package com.oms.db.seed;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oms.bean.Account;
import com.oms.bean.Station;
import com.oms.bean.Vehicle;
import com.oms.bean.RentalBike;

public class Seed {
    private ArrayList<Vehicle> vehicles;
    private ArrayList<RentalBike> rentalBikes;
    private ArrayList<Station> stations;
    private ArrayList<Account> accounts;

    private static Seed singleton = new Seed();

    private Seed() {
        start();
    }

    public static Seed singleton() {
        return singleton;
    }

    private void start() {
        vehicles = new ArrayList<Vehicle>();
        vehicles.addAll(generateDataFromFile(new File(getClass().getResource("./bike.json").getPath()).toString()));
        vehicles.addAll(generateDataFromFile(new File(getClass().getResource("./ebike.json").getPath()).toString()));
        vehicles.addAll(generateDataFromFile(new File(getClass().getResource("./twinbike.json").getPath()).toString()));
        stations = new ArrayList<Station>();
        accounts = new ArrayList<Account>();
        stations.addAll(generateDataStation(new File(getClass().getResource("./station.json").getPath()).toString()));
        accounts.addAll(generateDataAccount(new File(getClass().getResource("./account.json").getPath()).toString()));
        rentalBikes = new ArrayList<RentalBike>();
//        rentalBikes.addAll()
    }

    private ArrayList<? extends Vehicle> generateDataFromFile(String filePath) {
        ArrayList<? extends Vehicle> res = new ArrayList<Vehicle>();
        ObjectMapper mapper = new ObjectMapper();

        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
            res = mapper.readValue(json, new TypeReference<ArrayList<Vehicle>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }

        return res;
    }

    private ArrayList<? extends Station> generateDataStation(String filePath) {
        ArrayList<? extends Station> res = new ArrayList<Station>();
        ObjectMapper mapper = new ObjectMapper();

        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
            res = mapper.readValue(json, new TypeReference<ArrayList<Station>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }

        return res;
    }

    private ArrayList<? extends Account> generateDataAccount(String filePath) {
        ArrayList<? extends Account> res = new ArrayList<Account>();
        ObjectMapper mapper = new ObjectMapper();

        String json = FileReader.read(filePath);
        try {
            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
            res = mapper.readValue(json, new TypeReference<ArrayList<Account>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }

        return res;
    }

//    private ArrayList<? extends Station> generateDataRent(String filePath) {
//        ArrayList<? extends RentalBike> res = new ArrayList<RentalBike>();
//        ObjectMapper mapper = new ObjectMapper();
//
//        String json = FileReader.read(filePath);
//        try {
//            mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
//            res = mapper.readValue(json, new TypeReference<ArrayList<RentalBike>>() {
//            });
//        } catch (IOException e) {
//            e.printStackTrace();
//            System.out.println("Invalid JSON input data from " + filePath);
//        }
//
//        return res;
//    }

    public ArrayList<Vehicle> getVehicles() {
        return vehicles;
    }

    public ArrayList<RentalBike> getRentalBikes() {
        return rentalBikes;
    }

    public ArrayList<Station> getStations() {
        return stations;
    }

    public ArrayList<Account> getAccounts() {
        return accounts;
    }

    public static void main(String[] args) {
        Seed seed = new Seed();
        seed.start();
    }
}
