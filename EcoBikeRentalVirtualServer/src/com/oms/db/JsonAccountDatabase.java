package com.oms.db;

import com.oms.bean.Account;
import com.oms.db.seed.Seed;

import java.util.ArrayList;

public class JsonAccountDatabase implements IAccountDatabase {
    private static IAccountDatabase singleton = new JsonAccountDatabase();
    private ArrayList<Account> accounts = Seed.singleton().getAccounts();

    private JsonAccountDatabase() {
    }

    public static IAccountDatabase singleton() {
        return singleton;
    }

//    @Override
//    public ArrayList<Station> searchStation(Station station) {
//        ArrayList<Station> res = new ArrayList<Station>();
//        for (Station b : stations) {
//            if (b.match(station)) {
//                res.add(b);
//            }
//        }
//        return res;
//    }

    @Override
    public Account plusMoney(Account account, float money) {

        return account;
    }

    @Override
    public Account minusMoney(Account account, float money) {
        ArrayList<Account> res = new ArrayList<Account>();
        for (Account b : accounts) {
            if (b.match(account)) {
                res.add(b);
            }
        }
        if (res.size() > 0) {
            float balance = res.get(0).getBalance();
            if (balance >= money) {
                res.get(0).setBalance(balance - money);
                return res.get(0);
            }
        }
        return null;
    }

    @Override
    public float getBalance(String cardId, String password) {
        Account account = new Account(cardId, null, password);
        ArrayList<Account> res = new ArrayList<Account>();
        for (Account b : accounts) {
            if (b.match(account)) {
                res.add(b);
            }
        }
        if (res.size() > 0)
            return res.get(0).getBalance();
        else return 0.0f;
    }

    @Override
    public Account getBalance(Account account) {
        ArrayList<Account> res = new ArrayList<Account>();
        for (Account b : accounts) {
            if (b.match(account)) {
                res.add(b);
            }
        }
        if (res.size() > 0)
            return res.get(0);
        else return null;
    }
}
