package com.oms.db;

import com.oms.bean.Account;

public interface IAccountDatabase {
    public Account plusMoney(Account account, float money);

    public Account minusMoney(Account account, float money);

    public float getBalance(String cardId, String password);

    public Account getBalance(Account account);
}
