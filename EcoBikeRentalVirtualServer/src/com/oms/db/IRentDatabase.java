package com.oms.db;

import com.oms.bean.Bike;
import com.oms.bean.RentalBike;
import com.oms.bean.Vehicle;

import java.util.ArrayList;

public interface IRentDatabase {
    public RentalBike searchRent(RentalBike rentalBike);

    public RentalBike rentVehicle(RentalBike rentalBike);

    public RentalBike returnVehicle(RentalBike rentalBike);

    public RentalBike getVehicle(RentalBike rentalBike);
}
