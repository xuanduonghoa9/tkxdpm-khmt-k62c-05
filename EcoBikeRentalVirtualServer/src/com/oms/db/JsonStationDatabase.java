package com.oms.db;

import java.util.ArrayList;

import com.oms.bean.Station;
import com.oms.db.seed.Seed;

public class JsonStationDatabase implements IStationDatabase {
    private static IStationDatabase singleton = new JsonStationDatabase();

    private ArrayList<Station> stations = Seed.singleton().getStations();

    private JsonStationDatabase() {
    }

    public static IStationDatabase singleton() {
        return singleton;
    }

    @Override
    public ArrayList<Station> searchStation(Station station) {
        ArrayList<Station> res = new ArrayList<Station>();
        for (Station b : stations) {
            if (b.match(station)) {
                res.add(b);
            }
        }
        return res;
    }

    @Override
    public Station addStation(Station station) {
        for (Station m : stations) {
            if (m.equals(station)) {
                return null;
            }
        }

        stations.add(station);
        return station;
    }

    @Override
    public Station updateStation(Station station) {
        for (Station m : stations) {
            if (m.equals(station)) {
                stations.remove(m);
                stations.add(station);
                return station;
            }
        }
        return null;
    }

}
