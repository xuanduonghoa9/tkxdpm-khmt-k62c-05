package com.oms.service;

import com.oms.bean.Station;
import com.oms.db.IStationDatabase;
import com.oms.db.JsonStationDatabase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/stations")
public class StationService {
    private IStationDatabase stationDatabase;

    public StationService() {
        stationDatabase = JsonStationDatabase.singleton();
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Station> getAllStations(@QueryParam("stationId") String stationId, @QueryParam("stationName") String stationName,
                                             @QueryParam("stationAddress") String stationAddress, @QueryParam("numberOfDocks") Integer numberOfDocks) {
        Station station = new Station(stationId, stationName, stationAddress, numberOfDocks);
        station.setStationName(stationName);
//        station.setStationId(stationId);
        station.setStationAddress(stationAddress);
//        station.setNumberOfDocks(numberOfDocks);
        ArrayList<Station> res = stationDatabase.searchStation(station);
        return res;
    }
    
	@POST
	@Path("/{stationId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Station updateStation(@PathParam("stationId") String stationId, Station station) {
		return stationDatabase.updateStation(station);
	}
}
