package com.oms.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.Bike;
import com.oms.bean.Vehicle;
import com.oms.db.IVehicleDatabase;
import com.oms.db.JsonVehicleDatabase;

@Path("/bikes")
public class BikeService {

    private IVehicleDatabase vehicleDatabase;

    public BikeService() {
        vehicleDatabase = JsonVehicleDatabase.singleton();
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Vehicle> getBikes(@QueryParam("id") String id, @QueryParam("barcode") String barcode, @QueryParam("name") String name, @QueryParam("weight") Float weight,
                                       @QueryParam("licensePlate") String licensePlate, @QueryParam("cost") Double cost,
                                       @QueryParam("manufacturingDate") String manufacturingDate, @QueryParam("producer") String producer, @QueryParam("stationId") String stationId) {
        Bike bike = new Bike(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost, stationId);
        bike.setBarcode(barcode);
        bike.setId(id);
        bike.setCost(cost);
        bike.setLicensePlate(licensePlate);
        bike.setManufacturingDate(manufacturingDate);
        bike.setProducer(producer);
        bike.setName(name);
        bike.setWeight(weight);
        bike.setStationId(stationId);
        ArrayList<Vehicle> res = vehicleDatabase.searchVehicle(bike);
        return res;
    }

    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Vehicle updateBike(@PathParam("id") String id, Bike bike) {
        return vehicleDatabase.updateVehicle(bike);
    }
}