package com.oms.service;

import com.oms.bean.Account;
import com.oms.bean.Bike;
import com.oms.bean.Vehicle;
import com.oms.db.IAccountDatabase;
import com.oms.db.JsonAccountDatabase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/account")
public class AccountService {
    private IAccountDatabase accountDatabase;

    public AccountService() {
        accountDatabase = JsonAccountDatabase.singleton();
    }
    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Vehicle updateBike(@PathParam("id") String id, Bike bike) {
        return bike;
    }
    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Account> getBalance(@QueryParam("cardId") String cardId, @QueryParam("password") String password,
                                         @QueryParam("username") String username) {
        Account account = new Account(cardId, username, password);
        account.setCardId(cardId);
        account.setUsername(username);
        account.setPassword(password);
        float balance = accountDatabase.getBalance(cardId, password);
        account= accountDatabase.getBalance(account);
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(account);
        return accounts;
    }

    @GET
    @Path("/minus")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Account> minusBalance(@QueryParam("cardId") String cardId, @QueryParam("password") String password,
                                           @QueryParam("money") float money) {
        Account account = new Account();
        account.setCardId(cardId);
        account.setPassword(password);
        account= accountDatabase.minusMoney(account, money);
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(account);
        return accounts;
    }
}
