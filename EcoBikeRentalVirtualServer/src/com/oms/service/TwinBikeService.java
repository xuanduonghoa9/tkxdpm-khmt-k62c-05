package com.oms.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.*;
import com.oms.db.IVehicleDatabase;
import com.oms.db.JsonVehicleDatabase;

@Path("/twinbikes")
public class TwinBikeService {

    private IVehicleDatabase vehicleDatabase;

    public TwinBikeService() {
        vehicleDatabase = JsonVehicleDatabase.singleton();
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Vehicle> getTwinBikes(@QueryParam("id") String id, @QueryParam("barcode") String barcode, @QueryParam("name") String name, @QueryParam("weight") Float weight,
                                           @QueryParam("licensePlate") String licensePlate, @QueryParam("cost") Double cost,
                                           @QueryParam("manufacturingDate") String manufacturingDate, @QueryParam("producer") String producer, @QueryParam("stationId") String stationId) {
        TwinBike twinBike = new TwinBike(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost,  stationId);
        twinBike.setBarcode(barcode);
        twinBike.setId(id);
        twinBike.setCost(cost);
        twinBike.setLicensePlate(licensePlate);
        twinBike.setManufacturingDate(manufacturingDate);
        twinBike.setProducer(producer);
        twinBike.setName(name);
        twinBike.setWeight(weight);
        twinBike.setStationId(stationId);
        ArrayList<Vehicle> res = vehicleDatabase.searchVehicle(twinBike);
        return res;
    }

    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Vehicle updateTwinBike(@PathParam("id") String id, TwinBike bike) {
        return vehicleDatabase.updateVehicle(bike);
    }
}