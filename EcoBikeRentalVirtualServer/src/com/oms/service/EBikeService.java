package com.oms.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.*;
import com.oms.db.IVehicleDatabase;
import com.oms.db.JsonVehicleDatabase;

@Path("/ebikes")
public class EBikeService {

	private IVehicleDatabase vehicleDatabase;

	public EBikeService() {
		vehicleDatabase = JsonVehicleDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Vehicle> getEBikes(@QueryParam("id") String id, @QueryParam("barcode") String barcode, @QueryParam("name") String name, @QueryParam("weight") Float weight,
									   @QueryParam("licensePlate") String licensePlate, @QueryParam("cost") Double cost,
									   @QueryParam("manufacturingDate") String manufacturingDate, @QueryParam("producer") String producer, @QueryParam("stationId") String stationId
			, @QueryParam("stationId") Integer batteryPercentage, @QueryParam("stationId") Integer loadCycles, @QueryParam("stationId") Double estimatedUsageTimeRemaining) {
		EBike eBike = new EBike(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost,  stationId, batteryPercentage, loadCycles, estimatedUsageTimeRemaining);
		eBike.setBarcode(barcode);
		eBike.setId(id);
		eBike.setCost(cost);
		eBike.setLicensePlate(licensePlate);
		eBike.setManufacturingDate(manufacturingDate);
		eBike.setProducer(producer);
		eBike.setName(name);
		eBike.setWeight(weight);
		eBike.setBatteryPercentage(batteryPercentage);
		eBike.setEstimatedUsageTimeRemaining(estimatedUsageTimeRemaining);
		eBike.setLoadCycles(loadCycles);
		eBike.setStationId(stationId);
		ArrayList<Vehicle> res = vehicleDatabase.searchVehicle(eBike);
		return res;
	}

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Vehicle updateEBike(@PathParam("id") String id, EBike bike) {
		return vehicleDatabase.updateVehicle(bike);
	}
}