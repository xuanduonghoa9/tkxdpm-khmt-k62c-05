package com.oms.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.Vehicle;
import com.oms.db.IVehicleDatabase;
import com.oms.db.JsonVehicleDatabase;

@Path("/vehicles")
public class VehicleService {
	
	private IVehicleDatabase vehicleDatabase;
	
	public VehicleService() {
		vehicleDatabase = JsonVehicleDatabase.singleton();
	}

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Vehicle> getAllVehicles() {
    	ArrayList<Vehicle> res = vehicleDatabase.searchVehicle(null);
        return res;
    }
}