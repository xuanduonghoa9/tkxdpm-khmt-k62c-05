package com.oms.service;

import com.oms.bean.Bike;
import com.oms.bean.RentalBike;
import com.oms.bean.Vehicle;
import com.oms.db.IRentDatabase;
import com.oms.db.JsonRentDatabase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
@Path("/rent")
public class RentService {
    private IRentDatabase rentDatabase;

    public RentService() {
        rentDatabase = JsonRentDatabase.singleton();
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public RentalBike rentBikes(@QueryParam("id") String id, @QueryParam("barcode") String barcode, @QueryParam("customerName") String customerName, @QueryParam("customerCard") String customerCard,
                                @QueryParam("checkin") Date checkin,
                                @QueryParam("rentBikeParkID") String rentBikeParkID) {
        RentalBike rentalBike = new RentalBike(id,barcode,customerName,customerCard,checkin,rentBikeParkID);
        RentalBike res = rentDatabase.rentVehicle(rentalBike);
        return res;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public RentalBike getRentBike(@QueryParam("id") String id, @QueryParam("barcode") String barcode, @QueryParam("customerName") String customerName, @QueryParam("customerCard") String customerCard,
                                  @QueryParam("checkin") Date checkin,
                                  @QueryParam("rentBikeParkID") String rentBikeParkID) {
        RentalBike rentalBike = new RentalBike(id,barcode,customerName,customerCard,checkin,rentBikeParkID);
        RentalBike res = rentDatabase.searchRent(rentalBike);
        return res;
    }
    @POST
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RentalBike rentBike(RentalBike rentalBike) {
//        RentalBike rentalBike1 = new RentalBike(id,barcode,customerName,customerCard,checkin,rentBikeParkID);
        try {
            return rentDatabase.rentVehicle(rentalBike);
        } catch (Exception e){
            return null;
        }
    }
    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RentalBike updateBike(@PathParam("id") String id, RentalBike rentalBike) {
        return rentDatabase.returnVehicle(rentalBike);
    }
}
