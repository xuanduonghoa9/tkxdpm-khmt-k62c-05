package com.oms.bean;

public class EBike extends BikeVehicle {
    private Integer batteryPercentage;
    private Integer loadCycles;
    private Double estimatedUsageTimeRemaining;

    @Override
    public String toString() {
        return
                ", batteryPercentage=" + batteryPercentage +
                        ", loadCycles=" + loadCycles +
                        ", estimatedUsageTimeRemaining=" + estimatedUsageTimeRemaining;
    }

//    public EBike() {
//    }

    public EBike( Integer batteryPercentage, Integer loadCycles, Double estimatedUsageTimeRemaining) {
        super();
        this.batteryPercentage = batteryPercentage;
        this.loadCycles = loadCycles;
        this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
    }

//    public EBike(String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost, String type, Integer batteryPercentage, Integer loadCycles, Double estimatedUsageTimeRemaining) {
//        super(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost, type);
//        this.batteryPercentage = batteryPercentage;
//        this.loadCycles = loadCycles;
//        this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
//    }


    public EBike(String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost, String stationId, Integer batteryPercentage, Integer loadCycles, Double estimatedUsageTimeRemaining) {
        super(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost, stationId);
        this.batteryPercentage = batteryPercentage;
        this.loadCycles = loadCycles;
        this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
    }

    public EBike() {
        super();
    }

//    public EBike(String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost, String type) {
//        super(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost, type);
//    }

    @Override
    public boolean match(Vehicle vehicle) {
        if (vehicle == null)
            return true;


        boolean res = super.match(vehicle);
        if (!res) {
            return false;
        }


        if (!(vehicle instanceof EBike))
            return false;
        EBike eBike = (EBike) vehicle;

        if (eBike.batteryPercentage != null && !this.batteryPercentage.equals(eBike.batteryPercentage)) {
            return false;
        }
        if (eBike.loadCycles != null && !this.loadCycles.equals(eBike.loadCycles)) {
            return false;
        }
        if (eBike.estimatedUsageTimeRemaining != null && !this.estimatedUsageTimeRemaining.equals(eBike.estimatedUsageTimeRemaining)) {
            return false;
        }
        return true;
    }

    public Integer getBatteryPercentage() {
        return batteryPercentage;
    }

    public void setBatteryPercentage(Integer batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public Integer getLoadCycles() {
        return loadCycles;
    }

    public void setLoadCycles(Integer loadCycles) {
        this.loadCycles = loadCycles;
    }

    public Double getEstimatedUsageTimeRemaining() {
        return estimatedUsageTimeRemaining;
    }

    public void setEstimatedUsageTimeRemaining(Double estimatedUsageTimeRemaining) {
        this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
    }
}
