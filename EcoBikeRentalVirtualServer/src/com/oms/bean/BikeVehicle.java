package com.oms.bean;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = Bike.class, name = "bike"), @JsonSubTypes.Type(value = EBike.class, name = "ebike"), @JsonSubTypes.Type(value = TwinBike.class, name = "twinbike")})
public class BikeVehicle extends Vehicle {
    private String stationId;

    public BikeVehicle() {
    }


    public BikeVehicle(String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost, String stationId) {
        super(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost);
        this.stationId = stationId;
    }

    public BikeVehicle(String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost) {
        super(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost);
    }

    @Override
    public String toString() {
        return super.toString() +
                ", stationId='" + stationId + '\'';
    }

    @Override
    public boolean match(Vehicle vehicle) {
        if (vehicle == null)
            return true;


        boolean res = super.match(vehicle);
        if (!res) {
            return false;
        }


        if (!(vehicle instanceof BikeVehicle))
            return false;
        BikeVehicle bikeVehicle = (BikeVehicle) vehicle;
        if (bikeVehicle.stationId != null && !bikeVehicle.stationId.equals("") && !this.stationId.contains(bikeVehicle.stationId)) {
            return false;
        }
        return true;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

}
