package com.oms.bean;

import java.util.Date;

public class RentalBike {
    private String id;
    private String barcode;
    private String customerName;
    private String customerCard;
    private Date checkin;
    private Date checkout;
    private String returnBikeParkID;
    private String rentBikeParkID;
    private Double totalCost;

    public RentalBike(String id, String barcode, String customerName, String customerCard, Date checkin, String rentBikeParkID) {
        this.id = id;
        this.barcode = barcode;
        this.customerName = customerName;
        this.customerCard = customerCard;
        this.checkin = checkin;
        this.rentBikeParkID = rentBikeParkID;
    }

    public RentalBike(String id, String customerName, String customerCard, Date checkout, String returnBikeParkID) {
        this.id = id;
        this.customerName = customerName;
        this.customerCard = customerCard;
        this.checkout = checkout;
        this.returnBikeParkID = returnBikeParkID;
    }

    public RentalBike(String id, String barcode, String customerName, String customerCard, Date checkin, Date checkout, String returnBikeParkID, String rentBikeParkID, Double totalCost) {
        this.id = id;
        this.barcode = barcode;
        this.customerName = customerName;
        this.customerCard = customerCard;
        this.checkin = checkin;
        this.checkout = checkout;
        this.returnBikeParkID = returnBikeParkID;
        this.rentBikeParkID = rentBikeParkID;
        this.totalCost = totalCost;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RentalBike) {
            return this.barcode.equals(((RentalBike) obj).barcode);
        }
        return false;
    }

    public boolean search(RentalBike rentalBike) {
        if (this.id != null && !this.id.equals("") && !this.id.contains(rentalBike.id)) {
            return false;
        }
        if (this.barcode != null && !this.barcode.equals("") && !this.barcode.contains(rentalBike.barcode)) {
            return false;
        }
        if (this.customerName != null && !this.customerName.equals("") && !this.customerName.contains(rentalBike.customerName)) {
            return false;
        }
        if (this.customerCard != null && !this.customerCard.equals("") && !this.customerCard.contains(rentalBike.customerCard)) {
            return false;
        }
        if (this.totalCost != 0 && this.totalCost != rentalBike.totalCost) {
            return false;
        }
        return true;
    }

    public boolean match(RentalBike rentalBike) {
        if (rentalBike == null)
            return true;

        if (rentalBike.id != null && !rentalBike.id.equals("") && !this.id.contains(rentalBike.id)) {
            return false;
        }
        if (rentalBike.barcode != null && !rentalBike.barcode.equals("") && !this.barcode.contains(rentalBike.barcode)) {
            return false;
        }
        if (rentalBike.customerCard != null && !rentalBike.customerCard.equals("") && !this.customerCard.contains(rentalBike.customerCard)) {
            return false;
        }
        if (rentalBike.customerName != null && !rentalBike.customerName.equals("") && !this.customerName.contains(rentalBike.customerName)) {
            return false;
        }
        if (rentalBike.checkin != null) {
            return false;
        }
        if (rentalBike.checkout != null) {
            return false;
        }
        if (rentalBike.totalCost != null && !this.totalCost.equals(rentalBike.totalCost)) {
            return false;
        }
        if (rentalBike.rentBikeParkID != null && !this.rentBikeParkID.equals(rentalBike.rentBikeParkID)) {
            return false;
        }
        return true;
    }

    public Date getCheckin() {
        return checkin;
    }

    public void setCheckin(Date checkin) {
        this.checkin = checkin;
    }

    public Date getCheckout() {
        return checkout;
    }

    public void setCheckout(Date checkout) {
        this.checkout = checkout;
    }

    public String getReturnBikeParkID() {
        return returnBikeParkID;
    }

    public void setReturnBikeParkID(String returnBikeParkID) {
        this.returnBikeParkID = returnBikeParkID;
    }

    public String getRentBikeParkID() {
        return rentBikeParkID;
    }

    public void setRentBikeParkID(String rentBikeParkID) {
        this.rentBikeParkID = rentBikeParkID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCard() {
        return customerCard;
    }

    public void setCustomerCard(String customerCard) {
        this.customerCard = customerCard;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }
}
