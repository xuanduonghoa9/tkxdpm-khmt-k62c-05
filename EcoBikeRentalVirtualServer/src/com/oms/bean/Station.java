package com.oms.bean;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("station")
public class Station {
    private String stationId;
    private String stationName;
    private String stationAddress;
    private Integer numberOfBikes;
    private Integer numberOfEBikes;
    private Integer numberOfTwinBikes;
    private Integer numberOfEmpty;
    private Integer numberOfDocks;

    public Station() {
        super();
    }

    public Station(String stationId, String stationName, String stationAddress, Integer numberOfBikes, Integer numberOfEBikes, Integer numberOfTwinBikes, Integer numberOfEmpty) {
    	this.stationId = stationId;
        this.stationName = stationName;
        this.stationAddress = stationAddress;
        this.numberOfBikes = numberOfBikes;
        this.numberOfEBikes = numberOfEBikes;
        this.numberOfTwinBikes = numberOfTwinBikes;
        this.numberOfEmpty = numberOfEmpty;
    }


	public Station(String stationId, String stationName, String stationAddress, Integer numberOfDocks) {
        this.stationId = stationId;
        this.stationName = stationName;
        this.stationAddress = stationAddress;
        this.numberOfDocks = numberOfDocks;
    }

    @Override
    public boolean equals(Object obj) {
    	if (obj instanceof Station) {
			return this.stationId.equals(((Station) obj).stationId);
		}
		return false;
    }

    @Override
    public String toString() {
        return "Station{" +
                "stationId='" + stationId + '\'' +
                ", stationName='" + stationName + '\'' +
                ", stationAddress='" + stationAddress + '\'' +
                ", numberOfBikes=" + numberOfBikes +
                ", numberOfEBikes=" + numberOfEBikes +
                ", numberOfTwinBikes=" + numberOfTwinBikes +
                ", numberOfEmpty=" + numberOfEmpty +
                ", numberOfDocks=" + numberOfDocks +
                '}';
    }

    public boolean match(Station station) {
        if (station == null)
            return true;

        if (station.stationId != null && !station.stationId.equals("") && !this.stationId.contains(station.stationId)) {
            return false;
        }
        if (station.stationName != null && !station.stationName.equals("") && !this.stationName.contains(station.stationName)) {
            return false;
        }
        if (station.stationAddress != null && !station.stationAddress.equals("") && !this.stationAddress.contains(station.stationAddress)) {
            return false;
        }
//        if (station.numberOfBikes != 0 && !this.numberOfBikes.equals(station.numberOfBikes)) {
//            return false;
//        }
//        if (station.numberOfEBikes != 0 && !this.numberOfEBikes.equals(station.numberOfEBikes)) {
//            return false;
//        }
//        if (station.numberOfTwinBikes != 0 && !this.numberOfTwinBikes.equals(station.numberOfTwinBikes)) {
//            return false;
//        }
//        if (station.numberOfEmpty != 0 && !this.numberOfEmpty.equals(station.numberOfEmpty)) {
//            return false;
//        }
        return true;
    }

    public Integer getNumberOfDocks() {
        return numberOfDocks;
    }

    public void setNumberOfDocks(Integer numberOfDocks) {
        this.numberOfDocks = numberOfDocks;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationAddress() {
        return stationAddress;
    }

    public void setStationAddress(String stationAddress) {
        this.stationAddress = stationAddress;
    }

    public Integer getNumberOfBikes() {
        return numberOfBikes;
    }

    public void setNumberOfBikes(Integer numberOfBikes) {
        this.numberOfBikes = numberOfBikes;
    }

    public Integer getNumberOfEBikes() {
        return numberOfEBikes;
    }

    public void setNumberOfEBikes(Integer numberOfEBikes) {
        this.numberOfEBikes = numberOfEBikes;
    }

    public Integer getNumberOfTwinBikes() {
        return numberOfTwinBikes;
    }

    public void setNumberOfTwinBikes(Integer numberOfTwinBikes) {
        this.numberOfTwinBikes = numberOfTwinBikes;
    }

    public Integer getNumberOfEmpty() {
        return numberOfEmpty;
    }

    public void setNumberOfEmpty(Integer numberOfEmpty) {
        this.numberOfEmpty = numberOfEmpty;
    }
}
