package com.oms.bean;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("vehicle")
@JsonSubTypes({@JsonSubTypes.Type(value = BikeVehicle.class, name = "bikevehicle")})
public class Vehicle {
    private String id;
    private String barcode;
    private String name;
    private Float weight;
    private String licensePlate;
    private String manufacturingDate;
    private String producer;
    private Double cost;

    public Vehicle() {
        super();
    }

    public Vehicle(String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost) {
        this.id = id;
        this.barcode = barcode;
        this.name = name;
        this.weight = weight;
        this.licensePlate = licensePlate;
        this.manufacturingDate = manufacturingDate;
        this.producer = producer;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "id='" + id + '\'' +
                "barcode='" + barcode + '\'' +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                ", licensePlate='" + licensePlate + '\'' +
                ", manufacturingDate='" + manufacturingDate + '\'' +
                ", producer='" + producer + '\'' +
                ", cost=" + cost;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Vehicle) {
            return this.id.equals(((Vehicle) obj).id);
        }
        return false;
    }

    public boolean match(Vehicle vehicle) {
        if (vehicle == null)
            return true;

        if (vehicle.id != null && !vehicle.id.equals("") && !this.id.contains(vehicle.id)) {
            return false;
        }
        if (vehicle.barcode != null && !vehicle.barcode.equals("") && !this.barcode.contains(vehicle.barcode)) {
            return false;
        }
        if (vehicle.name != null && !vehicle.name.equals("") && !this.name.contains(vehicle.name)) {
            return false;
        }
        if (vehicle.licensePlate != null && !vehicle.licensePlate.equals("") && !this.licensePlate.contains(vehicle.licensePlate)) {
            return false;
        }
        if (vehicle.manufacturingDate != null && !vehicle.manufacturingDate.equals("") && !this.manufacturingDate.contains(vehicle.manufacturingDate)) {
            return false;
        }
        if (vehicle.producer != null && !vehicle.producer.equals("") && !this.producer.contains(vehicle.producer)) {
            return false;
        }
        if (vehicle.weight != null && !this.weight.equals(vehicle.weight)) {
            return false;
        }
        if (vehicle.cost != null && !this.cost.equals(vehicle.cost)) {
            return false;
        }
        return true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(String manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }
}
