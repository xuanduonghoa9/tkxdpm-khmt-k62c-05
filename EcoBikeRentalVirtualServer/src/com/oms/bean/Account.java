package com.oms.bean;

public class Account {
    private String cardId;
    private String username;
    private float balance;
    private String password;
    public Account(){
        super();
    }
    public Account(String cardId, String username, String password) {
        this.cardId = cardId;
        this.username = username;
        this.password = password;
    }

    public Account(String cardId, String username, float balance, String password) {
        this.cardId = cardId;
        this.username = username;
        this.balance = balance;
        this.password = password;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public boolean match(Account account) {
        if (account == null)
            return true;

        if (account.cardId != null && !account.cardId.equals("") && !this.cardId.equals(account.cardId)) {
            return false;
        }
        if (account.password != null && !account.password.equals("") && !this.password.equals(account.password)) {
            return false;
        }
        if (this.cardId.equals(account.cardId) && this.password.equals(account.password)){
            return true;
        }
        return false;
    }
}
