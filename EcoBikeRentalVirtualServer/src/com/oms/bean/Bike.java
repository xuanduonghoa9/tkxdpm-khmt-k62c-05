package com.oms.bean;

public class Bike extends BikeVehicle{



    public Bike() {
        super();
    }


    public Bike(String id, String barcode, String name, Float weight, String licensePlate, String manufacturingDate, String producer, Double cost,  String stationId) {
        super(id, barcode, name, weight, licensePlate, manufacturingDate, producer, cost, stationId);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public boolean match(Vehicle vehicle) {
        if (vehicle == null)
            return true;


        boolean res = super.match(vehicle);
        if (!res) {
            return false;
        }


        if (!(vehicle instanceof Bike))
            return false;
        Bike bike = (Bike) vehicle;


        return true;
    }
}
