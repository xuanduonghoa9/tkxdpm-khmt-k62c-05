# Hệ thống cho thuê xe EcoBikeRental Nhóm 05 #


### Phân công công việc ###

* Hoa Xuân Dương 20173068: Thuê xe - Nhóm trưởng
* Đặng Văn Toàn 20173406: Xem thông tin xe
* Phùng Hà Dương 20173061: Sửa thông tin bãi xe
* Lương Tiến Mạnh 20173254: Sửa thông tin xe
* Nguyễn Văn Tuấn 20173440: Xem danh sách bãi xe  
* Hoàng Thu Hà 20173087: Tìm kiếm bãi xe
* Trần Duy Hải 20173091: Trả xe

### Hướng dẫn cài đặt ###

* Clone project 
* Add maven
* Run server trong thư mục EcoBikeRentalVirtualServer
* Run file EBRAdmin để chạy chức năng với vai trò admin
* Run file EBRUser để chạy chức năng với vai trò user
* Các file test nằm trong đường dẫn EcoBikeRental_05/test
